package _06design.R12_11;

/**
 Describes a country with a name, area and population.
 */
public class Country {

    private String name;
    private double area;
    private double population;

    /**
     Constructs a country.
     @param name the country name
     @param area the area
     @param population the population
     */
    public Country(String name, double area, double population) {}

    /**
     Gets the country name.
     @return the country name
     */
    public String getName() { return ""; }

    /**
     Gets the area.
     @return the area
     */
    public double getArea() { return 0; }

    /**
     Gets the population.
     @return the population
     */
    public double getPopulation() { return 0; }

}
