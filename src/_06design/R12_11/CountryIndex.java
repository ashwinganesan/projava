package _06design.R12_11;

import java.io.File;
import java.util.ArrayList;

/**
 Describes a country index which stores an arraylist of countries.
 */
public class CountryIndex {

    private ArrayList<Country> countries;

    /**
     Constructs a country index.
     @param inputFile the file path containing the country records
     */
    public CountryIndex(File inputFile) {}

    /**
     Prints the country with the largest area.
     */
    public void printLargestArea() {}

    /**
     Prints the country with the largest population.
     */
    public void printLargestPopulation() {}

    /**
     Prints the country with the largest population density.
     */
    public void printLargestPopulationDensity() {}

}
