package _06design.R12_10;

import java.util.ArrayList;
import java.util.Scanner;

public class Quiz {

    private ArrayList<Question> questions;

    public Quiz() {
        questions = new ArrayList<Question>();
    }

    public void addQuestion(Question q) {
        questions.add(q);
    }

    public void presentQuestions() {
        Scanner in = new Scanner(System.in);

        for (Question q : questions) {
            q.display();
            System.out.print("Your answer: ");
            String response = in.nextLine();
            System.out.println(q.checkAnswer(response));
        }
    }

}
