package _06design.P12_1;

import java.util.ArrayList;
import java.util.HashMap;

public class VendingMachine {

    // Initialize arraylist for unique products
    private ArrayList<Product> products;

    // Initialize hashmap for product names and quantities
    private HashMap<String, Integer> productQuantities;

    // Initialize arraylist for coins
    private ArrayList<Coin> coins;

    // Initialize variables for input value, required value and number of coins entered
    private int inputValue;
    private int requiredValue;
    private int numCoins;

    // Constructor
    public VendingMachine(){
        products = new ArrayList<>();
        productQuantities = new HashMap<>();
        coins = new ArrayList<>();
        inputValue = 0;
        requiredValue = 0;
        numCoins = 0;
    }

    // Get products arraylist
    public ArrayList<Product> getProducts() {
        return products;
    }

    // Get product quantities hashmap
    public HashMap<String, Integer> getProductQuantities() {
        return productQuantities;
    }

    // Get input value
    public int getInputValue() {
        return inputValue;
    }

    // Get required value
    public int getRequiredValue() {
        return requiredValue;
    }

    // Get remaining value
    public int getRemainingValue() {
        return requiredValue - inputValue;
    }

    // Set input value
    public void setInputValue(int inputValue) {
        this.inputValue = inputValue;
    }

    // Set required value
    public void setRequiredValue(int requiredValue) {
        this.requiredValue = requiredValue;
        this.inputValue = 0;
    }

    // Set number of coins
    public void setNumCoins(int numCoins) {
        this.numCoins = numCoins;
    }

    // Add product to hashmap and arraylist
    public void addProduct(Product product) {
        boolean alreadyPresent = false;
        // If product present in hashmap, increment quantity by 1
        for (String productInside : productQuantities.keySet()) {
            if (product.getName().equals(productInside)) {
                productQuantities.put(productInside, productQuantities.get(productInside) + 1);
                alreadyPresent = true;
            }
        }
        // If product not present in hashmap, add to hashmap
        if (!alreadyPresent) {
            productQuantities.put(product.getName(), 1);
            // If product not present in arraylist, add to arraylist
            if (!products.contains(product)) {
                products.add(product);
            }
        }
    }

    // Remove product from hashmap
    public void removeProduct(String productName) {
        productQuantities.put(productName, productQuantities.get(productName) - 1);
        // If product quantity decreases to 0, remove from hashmap but keep in arraylist
        if (productQuantities.get(productName) == 0) {
            productQuantities.remove(productName);
        }
    }

    // Add coin to hashmap and arraylist
    public void addCoin(Coin coin) {
        coins.add(coin);

        // Increment input value by coin value
        inputValue += coin.getValue();
        numCoins += 1;
    }

    // Remove coins entered and return total value removed
    public int returnCoins() {
        int returnValue = 0;
        for (int i = 0; i < numCoins; i++) {
            returnValue += coins.get(coins.size() - 1).getValue();
            coins.remove(coins.size() - 1);
        }
        numCoins = 0;
        return returnValue;
    }

    // Empty vending machine
    int emptyMachine() {
        int returnValue = 0;
        for (int i = coins.size() - 1; i >= 0; i--) {
            returnValue += coins.get(i).getValue();
            coins.remove(i);
        }
        return returnValue;
    }

}
