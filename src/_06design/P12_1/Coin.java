package _06design.P12_1;

public class Coin {

    // Initialize name and value variables
    private String name;
    private int value;

    // Constructor
    public Coin(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // Get name
    public String getName() {
        return name;
    }

    // Get value
    public int getValue() {
        return value;
    }
}
