package _06design.P12_1;

import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {

        // Initialize vending machine object
        VendingMachine vendingMachine = new VendingMachine();

        // Add products to vending machine
        for (int i = 0; i < 5; i++) {
            vendingMachine.addProduct(new Product("SNICKERS", 50));
            vendingMachine.addProduct(new Product("MARS", 55));
            vendingMachine.addProduct(new Product("BOUNTY", 60));
            vendingMachine.addProduct(new Product("KITKAT", 70));
            vendingMachine.addProduct(new Product("TWIX", 80));
            vendingMachine.addProduct(new Product("KINDER", 95));
            vendingMachine.addProduct(new Product("FERRERO ROCHER", 120));
        }

        // Print product list with prices
        for (Product product : vendingMachine.getProducts()) {
            System.out.println(product.getName() + ": $" + String.format("%.2f", (float)product.getValue() / 100));
        }

        Scanner scan = new Scanner(System.in);
        String userInput = "";

        while (!userInput.equals("Q")) {
            // Get product name from user
            System.out.print("ENTER PRODUCT NAME: ");
            String productName = scan.nextLine();

            // Notify user if product is out of stock or doesn't exist
            if (!vendingMachine.getProductQuantities().containsKey(productName)) {
                System.out.println("PRODUCT OUT OF STOCK");
            }
            else {
                // Set required value based on product entered
                for (Product product : vendingMachine.getProducts()) {
                    if (product.getName().equals(productName)) {
                        vendingMachine.setRequiredValue(product.getValue());
                    }
                }

                // Print required value
                System.out.println("INSERT $" + String.format("%.2f", (float) vendingMachine.getRemainingValue() / 100));

                // Set number of coins added to 0
                vendingMachine.setNumCoins(0);

                // Add coins while input value is less than remaining value
                while (vendingMachine.getInputValue() < vendingMachine.getRequiredValue()) {

                    System.out.print("ENTER COIN NAME (CENT/NICKEL/DIME/QUARTER/HALF/DOLLAR), Q TO QUIT: ");
                    String coinName = scan.nextLine();

                    // Return coins if user quits
                    if (coinName.equals("Q")) {
                        System.out.println("$" + String.format("%.2f", (float) vendingMachine.returnCoins() / 100) + " RETURNED");
                        vendingMachine.setInputValue(0);
                        break;
                    }

                    else {
                        // Add coin based on coin entered
                        if (coinName.equals("CENT")) {
                            vendingMachine.addCoin(new Coin(coinName, 1));
                        } else if (coinName.equals("NICKEL")) {
                            vendingMachine.addCoin(new Coin(coinName, 5));
                        } else if (coinName.equals("DIME")) {
                            vendingMachine.addCoin(new Coin(coinName, 10));
                        } else if (coinName.equals("QUARTER")) {
                            vendingMachine.addCoin(new Coin(coinName, 20));
                        } else if (coinName.equals("HALF")) {
                            vendingMachine.addCoin(new Coin(coinName, 50));
                        } else if (coinName.equals("DOLLAR")) {
                            vendingMachine.addCoin(new Coin(coinName, 100));
                        } else {
                            System.out.println("INVALID COIN");
                        }


                        // Print remaining value
                        if (vendingMachine.getRemainingValue() > 0) {
                            System.out.println("INSERT $" + String.format("%.2f", (float) vendingMachine.getRemainingValue() / 100));
                        }

                    }

                }

                // Dispense product
                if (vendingMachine.getInputValue() >= vendingMachine.getRequiredValue()) {
                    vendingMachine.removeProduct(productName);
                    System.out.println(productName + " DISPENSED!");
                }

            }

            // Notify user to quit or continue
            System.out.print("TYPE Q TO QUIT OR ANYTHING ELSE TO CONTINUE: ");
            userInput = scan.nextLine();
        }

        // Thank user
        System.out.println("THANK YOU!");

        // Empty vending machine
        // System.out.println("$" + String.format("%.2f", (float) vendingMachine.emptyMachine() / 100) + " REMOVED");

    }

}
