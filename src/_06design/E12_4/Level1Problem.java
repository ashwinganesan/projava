package _06design.E12_4;

import java.util.Random;

public class Level1Problem extends Problem {

    public Level1Problem() {

        Random rand = new Random();

        // Random integer from 1 to 8
        int randInt1 = 1 + rand.nextInt(7);

        // Random integer from 1 to 9 - randInt1
        int randInt2 = 1 + rand.nextInt(8 - randInt1);

        // Set text and answer
        super.setText(randInt1 + " + " + randInt2 + " = ");
        super.setAnswer(randInt1 + randInt2);

    }

}
