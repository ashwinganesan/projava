package _06design.E12_4;

import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {

        // Create new ArithmeticProgram object
        ArithmeticProgram aProgram = new ArithmeticProgram();


        // Print level 1
        System.out.println("Level " + aProgram.getLevel());

        // Carry out game until player wins
        while (true) {

            // Add new problem if tries is set to 0
            if (aProgram.getTries() == 0) {
                aProgram.addProblem();
            }

            // Display problem and get input from user
            Scanner scan = new Scanner(System.in);
            aProgram.displayProblem();
            while(!scan.hasNextInt()) {
                System.out.println("Please type a number!");
                scan.nextLine();
                aProgram.displayProblem();
            }
            int answer = scan.nextInt();

            // Add point and set tries to 0 if input is correct
            if (aProgram.checkAnswer(answer)) {
                System.out.println("Correct!");
                aProgram.setTries(0);
                aProgram.setPoints(aProgram.getPoints() + 1);

                // Move to next level if 5 points reached
                if (aProgram.getPoints() == 5) {
                    aProgram.setLevel(aProgram.getLevel() + 1);
                    aProgram.setPoints(0);

                    // Break out of game if level 3 completed
                    if (aProgram.getLevel() == 4) {
                        System.out.println("You win!");
                        break;
                    }

                    // Print new level
                    System.out.println("Level " + aProgram.getLevel());
                }
            }

            // If input is wrong, provide another try
            else {
                System.out.println("Wrong!");
                aProgram.setTries(aProgram.getTries() + 1);

                // If two tries provided, move to next problem by setting tries to 0
                if (aProgram.getTries() == 2) {
                    aProgram.setTries(0);
                }
            }
        }

    }

}
