package _06design.E12_4;

public class Problem {

    // Initialize text and answer variables
    private String text;
    private int answer;

    // Get text
    public String getText() {
        return text;
    }

    // Get answer
    public int getAnswer() {
        return answer;
    }

    // Set text
    public void setText(String text) {
        this.text = text;
    }

    // Set answer
    public void setAnswer(int answer) {
        this.answer = answer;
    }

}
