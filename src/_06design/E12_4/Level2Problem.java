package _06design.E12_4;

import java.util.Random;

public class Level2Problem extends Problem {

    public Level2Problem() {

        Random rand = new Random();

        // Random integer from 1 to 9
        int randInt1 = 1 + rand.nextInt(8);

        // Random integer from 1 to 9
        int randInt2 = 1 + rand.nextInt(8);

        // Set text and answer
        super.setText(randInt1 + " + " + randInt2 + " = ");
        super.setAnswer(randInt1 + randInt2);

    }

}
