package _06design.E12_4;

import java.util.Random;

public class Level3Problem extends Problem {

    public Level3Problem() {

        Random rand = new Random();

        // Random integer from 2 to 9
        int randInt1 = 2 + rand.nextInt(7);

        // Random integer from 1 to randInt
        int randInt2 = 1 + rand.nextInt(randInt1 - 1);

        // Set text and answer
        super.setText(randInt1 + " - " + randInt2 + " = ");
        super.setAnswer(randInt1 - randInt2);

    }

}
