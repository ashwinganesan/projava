package _06design.E12_4;

import java.util.ArrayList;

public class ArithmeticProgram {

    // Initialize arraylist of problems, level, points and tries variables
    private ArrayList<Problem> problems;
    private int level;
    private int points;
    private int tries;

    // Constructor
    public ArithmeticProgram() {
        problems = new ArrayList<>();
        level = 1;
        points = 0;
        tries = 0;
    }

    // Get level
    public int getLevel() {
        return level;
    }

    // Get points
    public int getPoints() {
        return points;
    }

    // Get tries
    public int getTries() {
        return tries;
    }

    // Set level
    public void setLevel(int level) {
        this.level = level;
    }

    // Set points
    public void setPoints(int points) {
        this.points = points;
    }

    // Set tries
    public void setTries(int tries) {
        this.tries = tries;
    }

    // Add problem depending on level
    public void addProblem() {
        switch (level) {
            case 1:
                problems.add(new Level1Problem());
                break;
            case 2:
                problems.add(new Level2Problem());
                break;
            case 3:
                problems.add(new Level3Problem());
                break;
        }
    }

    // Display last problem in arraylist
    public void displayProblem() {
        System.out.print(problems.get(problems.size() - 1).getText());
    }

    // Check answer for last problem in arraylist
    public boolean checkAnswer(int answer) {
        return problems.get(problems.size() - 1).getAnswer() == answer;
    }

}
