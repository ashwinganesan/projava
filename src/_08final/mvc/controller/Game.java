package _08final.mvc.controller;

import _08final.mvc.model.*;
import _08final.mvc.view.GamePanel;
import _08final.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(1100, 900); //the dimension of the game.
	private GamePanel gmpPanel;
	public static Random R = new Random();
	public final static int ANI_DELAY = 45; // milliseconds between screen
											// updates (animation)
	private Thread thrAnim;
	private int nLevel = 1;
	private int nTick = 0;

	private boolean bMuted = true;
	

	private final int PAUSE = 80, // p key
			QUIT = 81, // q key
			LEFT = 37, // rotate left; left arrow
			RIGHT = 39, // rotate right; right arrow
			UP = 38, // thrust; up arrow
			START = 83, // s key
			FIRE = 32, // space key
			MUTE = 77, // m key

	HYPER = 65, 					// hyperspace ; a key
	CRUISE = 70,					// fire cruise missile; f key
	STEERABLE = 69;					// hold for steerable missile; e key

	private Clip clpThrust;
	private Clip clpMusicBackground;

	private static final int SPAWN_UFO_FLOATER = 600;
	private static final int SPAWN_NEW_SHIP_FLOATER = 500;
	private static final int SPAWN_SHIELD_FLOATER = 400;
	private static final int SPAWN_EXPLOSIVE_BULLET_FLOATER = 300;

	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {

		gmpPanel = new GamePanel(DIM);
		gmpPanel.addKeyListener(this);
		clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
		clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");
	

	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
					public void run() {
						try {
							Game game = new Game(); // construct itself
							game.fireUpAnimThread();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void fireUpAnimThread() { // called initially
		if (thrAnim == null) {
			thrAnim = new Thread(this); // pass the thread a runnable object (this)
			thrAnim.start();
		}
	}

	// Implements runnable - must have run method
	public void run() {

		// Lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		thrAnim.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// This thread animates the scene
		while (Thread.currentThread() == thrAnim) {
			tick();
			spawnNewShipFloater();
			spawnShieldFloater();
			spawnExplosiveBulletFloater();
			spawnUfoFloater();
			gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must 
														// surround the sleep() in a try/catch block
														// this simply controls delay time between 
														// the frames of the animation

			// Check for collisions
			checkCollisions();

			// Check if level is clear
			checkNewLevel();

			// Check if steerable missile is present
			checkSteerableMissile();

			// Decrement explosive bullets left
			if (CommandCenter.getInstance().getExplosiveBulletLeft() > 0) {
			 	CommandCenter.getInstance().setExplosiveBulletLeft(CommandCenter.getInstance().getExplosiveBulletLeft() - 3);
			 }

			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// Just skip this frame -- no big deal
				continue;
			}
		}
	}

	private void checkCollisions() {

		Point pntFriendCenter, pntFoeCenter;
		int nFriendRadiux, nFoeRadiux;

		for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
			for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {

				pntFriendCenter = movFriend.getCenter();
				pntFoeCenter = movFoe.getCenter();
				nFriendRadiux = movFriend.getRadius();
				nFoeRadiux = movFoe.getRadius();

				// Detect collision
				if (pntFriendCenter.distance(pntFoeCenter) < (nFriendRadiux + nFoeRadiux)) {

					// Falcon
					if (movFriend instanceof Falcon){
						if (!CommandCenter.getInstance().getFalcon().getProtected()){
							CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
							CommandCenter.getInstance().spawnFalcon(false);

						}
					}
					// Not the falcon
					else {

						// Explosive bullet causes explosion on hitting small asteroid
						if (movFriend instanceof ExplosiveBullet && movFoe instanceof Asteroid && movFoe.getRadius() == 25) {
							CommandCenter.getInstance().getOpsList().enqueue(new Explosion(movFoe.getCenter()), CollisionOp.Operation.ADD);
						}

						CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
					}
					// Kill the foe and if asteroid, then spawn new asteroids
					killFoe(movFoe);
					Sound.playSound("kapow.wav");

				}
			}
		}

		// Check for collisions between falcon and floaters
		if (CommandCenter.getInstance().getFalcon() != null){
			Point pntFalCenter = CommandCenter.getInstance().getFalcon().getCenter();
			int nFalRadiux = CommandCenter.getInstance().getFalcon().getRadius();
			Point pntFloaterCenter;
			int nFloaterRadiux;
			
			for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
				pntFloaterCenter = movFloater.getCenter();
				nFloaterRadiux = movFloater.getRadius();
	
				// Detect collision
				if (pntFalCenter.distance(pntFloaterCenter) < (nFalRadiux + nFloaterRadiux)) {

					// Increment number of falcons if floater is new ship floater
					if (movFloater instanceof NewShipFloater) {
						CommandCenter.getInstance().setNumFalcons(CommandCenter.getInstance().getNumFalcons() + 1);
					}

					// Set shield if floater is shield floater
					else if (movFloater instanceof ShieldFloater) {
						CommandCenter.getInstance().getFalcon().setProtected(true);
						CommandCenter.getInstance().getFalcon().setShieldLeft(999);
					}

					// Set explosive bullets usable if floater is explosive bullets floater
					else if (movFloater instanceof ExplosiveBulletFloater) {
						CommandCenter.getInstance().setExplosiveBulletLeft(999);
					}

					// Kill falcon if floater is ufo floater
					else if (movFloater instanceof UfoFloater) {
						if (!CommandCenter.getInstance().getFalcon().getProtected()){
							CommandCenter.getInstance().getOpsList().enqueue(CommandCenter.getInstance().getFalcon(), CollisionOp.Operation.REMOVE);
							CommandCenter.getInstance().spawnFalcon(false);

						}
					}

					CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);
					Sound.playSound("pacman_eatghost.wav");
	
				}
			}
		}

		// We are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists while iterating them above
		while(!CommandCenter.getInstance().getOpsList().isEmpty()){
			CollisionOp cop =  CommandCenter.getInstance().getOpsList().dequeue();
			Movable mov = cop.getMovable();
			CollisionOp.Operation operation = cop.getOperation();

			switch (mov.getTeam()){
				case FOE:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFoes().add(mov);
					} else {
						CommandCenter.getInstance().getMovFoes().remove(mov);
					}

					break;
				case FRIEND:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFriends().add(mov);
					} else {
						CommandCenter.getInstance().getMovFriends().remove(mov);
					}
					break;

				case FLOATER:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFloaters().add(mov);
					} else {
						CommandCenter.getInstance().getMovFloaters().remove(mov);
					}
					break;

				case DEBRIS:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovDebris().add(mov);
					} else {
						CommandCenter.getInstance().getMovDebris().remove(mov);
					}
					break;

			}

		}
		// A request to the JVM is made every frame to garbage collect, however, the JVM will choose when and how to do this
		System.gc();
		
	}

	private void killFoe(Movable movFoe) {
		
		if (movFoe instanceof Asteroid){

			CommandCenter.getInstance().setScore(CommandCenter.getInstance().getScore() + 1);

			// We know this is an Asteroid, so we can cast without threat of ClassCastException
			Asteroid astExploded = (Asteroid)movFoe;
			// Big asteroid exploded
			if(astExploded.getSize() == 0){
				// Spawn two medium asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
			} 
			// Medium size asteroid exploded
			else if(astExploded.getSize() == 1){
				// Spawn three small asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
			}

			// Small size asteroid explodes into debris
			else if(astExploded.getSize() == 2){

				for (int angle = 0; angle < 360; angle += 45) {
					CommandCenter.getInstance().getOpsList().enqueue(new Debris(astExploded, angle), CollisionOp.Operation.ADD);
				}

			}

		} 

		// Remove the original Foe
		CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);

	}

	// Some methods for timing events in the game,
	// such as the appearance of UFOs, floaters (power-ups), etc.
	public void tick() {
		if (nTick == Integer.MAX_VALUE)
			nTick = 0;
		else
			nTick++;
	}

	public int getTick() {
		return nTick;
	}

	// Spawn new ship floater
	private void spawnNewShipFloater() {
		if (nTick % SPAWN_NEW_SHIP_FLOATER == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(), CollisionOp.Operation.ADD);
		}
	}

	// Spawn shield floater
	private void spawnShieldFloater() {
		if (nTick % SPAWN_SHIELD_FLOATER == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new ShieldFloater(), CollisionOp.Operation.ADD);
		}
	}

	// Spawn explosive bullet floater
	private void spawnExplosiveBulletFloater() {
		if (nTick % SPAWN_EXPLOSIVE_BULLET_FLOATER == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new ExplosiveBulletFloater(), CollisionOp.Operation.ADD);
		}
	}

	// Spawn ufo floater
	private void spawnUfoFloater() {
		if (nTick % SPAWN_UFO_FLOATER == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new UfoFloater(CommandCenter.getInstance().getFalcon()), CollisionOp.Operation.ADD);
		}
	}

	// Called when user presses 's'
	private void startGame() {
		CommandCenter.getInstance().clearAll();
		CommandCenter.getInstance().initGame();
		CommandCenter.getInstance().setLevel(0);
		CommandCenter.getInstance().setPlaying(true);
		CommandCenter.getInstance().setPaused(false);
	}

	// This method spawns new asteroids
	private void spawnAsteroids(int nNum) {
		for (int nC = 0; nC < nNum; nC++) {

			// Asteroids with size of zero are big
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

		}
	}

	private boolean isLevelClear(){
		// If there are no more asteroids on the screen
		boolean bAsteroidFree = true;
		for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
			if (movFoe instanceof Asteroid){
				bAsteroidFree = false;
				break;
			}
		}
		
		return bAsteroidFree;

	}
	
	private void checkNewLevel(){
		
		if (isLevelClear() ){
			if (CommandCenter.getInstance().getFalcon() !=null)
				CommandCenter.getInstance().getFalcon().setProtected(true);
			
			spawnAsteroids(CommandCenter.getInstance().getLevel() + 2);
			CommandCenter.getInstance().setLevel(CommandCenter.getInstance().getLevel() + 1);

		}
	}
	
	private void checkSteerableMissile() {

		// Set boolean for steerable missile to false if it crashes before key is released
		if (CommandCenter.getInstance().getSteerableMissile() == null) {
			CommandCenter.getInstance().setIsSteerableMissile(false);
		}

	}
	

	// Varargs for stopping looping-music-clips
	private static void stopLoopingSounds(Clip... clpClips) {
		for (Clip clp : clpClips) {
			clp.stop();
		}
	}

	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();

		if (nKey == START && !CommandCenter.getInstance().isPlaying())
			startGame();

		if (fal != null) {

			switch (nKey) {
			case PAUSE:
				CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
				if (CommandCenter.getInstance().isPaused())
					stopLoopingSounds(clpMusicBackground, clpThrust);
				else
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case QUIT:
				System.exit(0);
				break;
			case STEERABLE:
				if (!CommandCenter.getInstance().getIsSteerableMissile()) {
					CommandCenter.getInstance().getOpsList().enqueue(new SteerableMissile(fal), CollisionOp.Operation.ADD);
					CommandCenter.getInstance().setIsSteerableMissile(true);
				}
				break;

			// Give control to falcon or steerable missile depending on boolean
			case UP:
				if (!CommandCenter.getInstance().getIsSteerableMissile()) {
					fal.thrustOn();
				}
				if (!CommandCenter.getInstance().isPaused())
					clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case LEFT:
				if (!CommandCenter.getInstance().getIsSteerableMissile()) {
					fal.rotateLeft();
				}
				else {
					CommandCenter.getInstance().getSteerableMissile().rotateLeft();
				}
				break;
			case RIGHT:
				if (!CommandCenter.getInstance().getIsSteerableMissile()) {
					fal.rotateRight();
				}
				else {
					CommandCenter.getInstance().getSteerableMissile().rotateRight();
				}
				break;

			default:
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();

		if (fal != null) {
			switch (nKey) {
			case FIRE:
				if (CommandCenter.getInstance().getExplosiveBulletLeft() == 0) {
					CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal), CollisionOp.Operation.ADD);
				}
				else {
					CommandCenter.getInstance().getOpsList().enqueue(new ExplosiveBullet(fal), CollisionOp.Operation.ADD);
				}
				Sound.playSound("laser.wav");
				break;
				
			// Fires the cruise missile
			case CRUISE:
				CommandCenter.getInstance().getOpsList().enqueue(new CruiseMissile(fal), CollisionOp.Operation.ADD);
				//Sound.playSound("laser.wav");
				break;

			// Remove steerable missile on key release
			case STEERABLE:

				if (CommandCenter.getInstance().getSteerableMissile() != null) {
					CommandCenter.getInstance().getOpsList().enqueue(CommandCenter.getInstance().getSteerableMissile(), CollisionOp.Operation.REMOVE);
				}

				CommandCenter.getInstance().setIsSteerableMissile(false);
				break;

			// Stop movement of falcon or steerable missile depending on boolean
			case LEFT:
				if (!CommandCenter.getInstance().getIsSteerableMissile()) {
					fal.stopRotating();
				}
				else {
					CommandCenter.getInstance().getSteerableMissile().stopRotating();
				}
				break;

			case RIGHT:
				if (!CommandCenter.getInstance().getIsSteerableMissile()) {
					fal.stopRotating();
				}
				else {
					CommandCenter.getInstance().getSteerableMissile().stopRotating();
				}
				break;

			case UP:
				if (!CommandCenter.getInstance().getIsSteerableMissile()) {
					fal.thrustOff();
				}
				clpThrust.stop();
				break;
				
			case MUTE:
				if (!bMuted){
					stopLoopingSounds(clpMusicBackground);
					bMuted = !bMuted;
				} 
				else {
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
					bMuted = !bMuted;
				}
				break;

			case HYPER:
				// Teleport to random location
				CommandCenter.getInstance().getFalcon().setProtected(true);
				CommandCenter.getInstance().getFalcon().setCenter(new Point(Game.R.nextInt(Game.DIM.width), Game.R.nextInt(Game.DIM.height)));

			default:
				break;
			}
		}
	}

	@Override
	// Just need it b/c of KeyListener implementation
	public void keyTyped(KeyEvent e) {
	}

}