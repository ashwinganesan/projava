package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

public class SteerableMissile extends Sprite {

    final int DEGREE_STEP = 7;

    private final double FIRE_POWER = 15.0;

    private boolean bTurningRight = false;
    private boolean bTurningLeft = false;

    public SteerableMissile(Falcon fal) {

        super();
        setTeam(Team.FRIEND);
        // same shape as cruise missile
        ArrayList<Point> pntCs = new ArrayList<Point>();

        pntCs.add(new Point(0, 5));
        pntCs.add(new Point(1, 3));
        pntCs.add(new Point(1, 0));
        pntCs.add(new Point(6, 0));
        pntCs.add(new Point(6, -1));
        pntCs.add(new Point(1, -1));
        pntCs.add(new Point(1, -2));

        pntCs.add(new Point(-1, -2));
        pntCs.add(new Point(-1, -1));
        pntCs.add(new Point(-6, -1));
        pntCs.add(new Point(-6, 0));
        pntCs.add(new Point(-1, 0));
        pntCs.add(new Point(-1, 3));
        assignPolarPoints(pntCs);

        setRadius(20);

        // everything is relative to the falcon ship that fired the bullet
        setDeltaX(fal.getDeltaX()
                + Math.cos(Math.toRadians(fal.getOrientation())) * FIRE_POWER);
        setDeltaY(fal.getDeltaY()
                + Math.sin(Math.toRadians(fal.getOrientation())) * FIRE_POWER);
        setCenter(fal.getCenter());

        // set the bullet orientation to the falcon (ship) orientation
        setOrientation(fal.getOrientation());
        setColor(Color.RED);

    }

    @Override
    public void move() {
        super.move();

        if (bTurningLeft) {

            if (getOrientation() <= 0 && bTurningLeft) {
                setOrientation(360);
            }
            setOrientation(getOrientation() - DEGREE_STEP);
        }
        if (bTurningRight) {
            if (getOrientation() >= 360 && bTurningRight) {
                setOrientation(0);
            }
            setOrientation(getOrientation() + DEGREE_STEP);
        }

        //set deltaX and deltaY based on current orientation
        setDeltaX(Math.cos(Math.toRadians(getOrientation())) * FIRE_POWER);
        setDeltaY(Math.sin(Math.toRadians(getOrientation())) * FIRE_POWER);
    }

    public void rotateLeft() {
        bTurningLeft = true;
    }

    public void rotateRight() {
        bTurningRight = true;
    }

    public void stopRotating() {
        bTurningRight = false;
        bTurningLeft = false;
    }

}
