package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

public class Debris extends Sprite {

    // movement speed of debris
    private final double SPEED = 30.0;

    // constructor takes exploded asteroid and orientation angle as inputs
    public Debris(Asteroid astExploded, int nOrientation) {

        super();
        setTeam(Team.DEBRIS);

        setExpire(20);
        setRadius(5);
        setOrientation(nOrientation);

        // draw circle with 40 points
        ArrayList<Point> pntCs = new ArrayList<Point>();

        for (double angle = 0; angle < 2 * Math.PI; angle += Math.PI / 20) {
            pntCs.add(new Point((int) (getRadius() * Math.cos(angle)),(int) (getRadius() * Math.sin(angle))));
        }

        assignPolarPoints(pntCs);

        setDeltaX(Math.cos(Math.toRadians(getOrientation())) * SPEED);
        setDeltaY(Math.sin(Math.toRadians(getOrientation())) * SPEED);
        setCenter(astExploded.getCenter());

    }

    @Override
    public void move(){

        super.move();

        if (getExpire() == 0)
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        else
            setExpire(getExpire() - 1);

    }

}
