package _08final.mvc.model;

import java.awt.*;

public class ExplosiveBullet extends Bullet {

    public ExplosiveBullet(Falcon fal) {
        super(fal);
        setColor(Color.red);
    }

}
