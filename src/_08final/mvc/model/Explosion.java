package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

public class Explosion extends Sprite {

    public Explosion(Point center) {

        super();
        setTeam(Team.FRIEND);
        setExpire(15);
        setRadius(5);
        setColor(Color.yellow);

        // draw circle with 40 points
        ArrayList<Point> pntCs = new ArrayList<Point>();

        for (double angle = 0; angle < 2 * Math.PI; angle += Math.PI / 20) {
            pntCs.add(new Point((int) (getRadius() * Math.cos(angle)),(int) (getRadius() * Math.sin(angle))));
        }

        assignPolarPoints(pntCs);
        setDeltaX(0);
        setDeltaY(0);
        setCenter(center);

    }

    @Override
    public void move(){

        super.move();

        if (getExpire() == 0)
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        else {
            setRadius(getRadius() + 5);

            // draw circle with 40 points
            ArrayList<Point> pntCs = new ArrayList<Point>();

            for (double angle = 0; angle < 2 * Math.PI; angle += Math.PI / 20) {
                pntCs.add(new Point((int) (getRadius() * Math.cos(angle)), (int) (getRadius() * Math.sin(angle))));
            }

            assignPolarPoints(pntCs);

            setExpire(getExpire() - 1);
        }
    }

}
