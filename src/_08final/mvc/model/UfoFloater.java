package _08final.mvc.model;

import java.awt.*;

public class UfoFloater extends Floater {

    private Falcon falcon;

    public UfoFloater(Falcon fal) {
        super();
        falcon = fal;
        setColor(Color.red);
        setExpire(333); // must be lower than spawn rate to have single ufo on screen
    }

    public void move() {
        super.move();

        // move towards falcon
        int deltaX = falcon.getCenter().x - this.getCenter().x;
        int deltaY = falcon.getCenter().y - this.getCenter().y;

        double angle = Math.atan2(deltaY, deltaX);
        setDeltaX(3 * Math.cos(angle));
        setDeltaY(3 * Math.sin(angle));
    }

}
