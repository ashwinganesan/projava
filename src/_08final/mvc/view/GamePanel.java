package _08final.mvc.view;

import _08final.mvc.controller.Game;
import _08final.mvc.model.CommandCenter;
import _08final.mvc.model.Falcon;
import _08final.mvc.model.Movable;

import java.awt.*;
import java.util.ArrayList;


public class GamePanel extends Panel {
	
	// ==============================================================
	// FIELDS 
	// ============================================================== 
	 
	// The following "off" vars are used for the off-screen double-buffered image
	private Dimension dimOff;
	private Image imgOff;
	private Graphics grpOff;
	
	private GameFrame gmf;
	private Font fnt = new Font("MS Gothic", Font.BOLD, 18);
	private Font fntBig = new Font("MS Gothic", Font.BOLD + Font.ITALIC, 72);
	private FontMetrics fmt; 
	private int nFontWidth;
	private int nFontHeight;
	private String strDisplay = "";
	

	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public GamePanel(Dimension dim){
	    gmf = new GameFrame();
		gmf.getContentPane().add(this);
		gmf.pack();
		initView();
		
		gmf.setSize(dim);
		gmf.setTitle("Game Base");
		gmf.setResizable(false);
		gmf.setVisible(true);
		this.setFocusable(true);
	}
	
	
	// ==============================================================
	// METHODS 
	// ==============================================================
	
	private void drawScore(Graphics g) {
		g.setColor(Color.white);
		g.setFont(fnt);

		// Print score
		if (CommandCenter.getInstance().getScore() != 0) {
			g.drawString("SCORE :  " + CommandCenter.getInstance().getScore(), nFontWidth, nFontHeight);
		} else {
			g.drawString("NO SCORE", nFontWidth, nFontHeight);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void update(Graphics g) {
		if (grpOff == null || Game.DIM.width != dimOff.width
				|| Game.DIM.height != dimOff.height) {
			dimOff = Game.DIM;
			imgOff = createImage(Game.DIM.width, Game.DIM.height);
			grpOff = imgOff.getGraphics();
		}
		// Fill in background with black
		grpOff.setColor(Color.black);
		grpOff.fillRect(0, 0, Game.DIM.width, Game.DIM.height);

		drawScore(grpOff);
		
		if (!CommandCenter.getInstance().isPlaying()) {
			displayTextOnScreen();
		} else if (CommandCenter.getInstance().isPaused()) {
			strDisplay = "Game Paused";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);
		}
		
		// Playing and not paused!
		else {
			
			// Draw them in decreasing level of importance
			// Friends will be on top layer and debris on the bottom
			iterateMovables(grpOff,
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFriends(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFoes(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFloaters(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovDebris());

			drawNumberShipsLeft(grpOff);
			drawMeters(grpOff);
			if (CommandCenter.getInstance().isGameOver()) {
				CommandCenter.getInstance().setPlaying(false);
			}
		}
		// Draw the double-Buffered Image to the graphics context of the panel
		g.drawImage(imgOff, 0, 0, this);
	} 

	// For each movable array, process it
	private void iterateMovables(Graphics g, ArrayList<Movable>...movMovz){
		
		for (ArrayList<Movable> movMovs : movMovz) {
			for (Movable mov : movMovs) {

				mov.move();
				mov.draw(g);

			}
		}
		
	}

	// Draw the number of falcons left on the bottom-right of the screen. 
	private void drawNumberShipsLeft(Graphics g) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		double[] dLens = fal.getLengths();
		int nLen = fal.getDegrees().length;
		Point[] pntMs = new Point[nLen];
		int[] nXs = new int[nLen];
		int[] nYs = new int[nLen];
	
		// Convert to cartesian points
		for (int nC = 0; nC < nLen; nC++) {
			pntMs[nC] = new Point((int) (10 * dLens[nC] * Math.sin(Math
					.toRadians(90) + fal.getDegrees()[nC])),
					(int) (10 * dLens[nC] * Math.cos(Math.toRadians(90)
							+ fal.getDegrees()[nC])));
		}
		
		// Set the color to white
		g.setColor(Color.green);
		// For each falcon left (not including the one that is playing)
		for (int nD = 1; nD < CommandCenter.getInstance().getNumFalcons(); nD++) {
			for (int nC = 0; nC < fal.getDegrees().length; nC++) {
				nXs[nC] = pntMs[nC].x + Game.DIM.width - (Game.DIM.width / 40 * nD);
				nYs[nC] = pntMs[nC].y + Game.DIM.height - Game.DIM.height / 15;
			}
			g.drawPolygon(nXs, nYs, nLen);
		} 
	}

	// Draw meters at bottom left of screen
	private void drawMeters(Graphics g) {

		// Shield meter
		g.setColor(Color.blue);
		g.fillRoundRect(Game.DIM.width / 120, Game.DIM.height - Game.DIM.height / 15,
				CommandCenter.getInstance().getFalcon().getShieldLeft() * (Game.DIM.width / 5) / 999,
				Game.DIM.height / 120, Game.DIM.height / 240, Game.DIM.height / 240);
		g.setColor(Color.white);
		g.drawRoundRect(Game.DIM.width / 120, Game.DIM.height - Game.DIM.height / 15, Game.DIM.width / 5,
				Game.DIM.height / 120, Game.DIM.height / 240, Game.DIM.height / 240);

		// Explosive bullets meter
		g.setColor(Color.yellow);
		g.fillRoundRect(2 * Game.DIM.width / 120 + Game.DIM.width / 5, Game.DIM.height - Game.DIM.height / 15,
				CommandCenter.getInstance().getExplosiveBulletLeft() * (Game.DIM.width / 5) / 999,
				Game.DIM.height / 120, Game.DIM.height / 240, Game.DIM.height / 240);
		g.setColor(Color.white);
		g.drawRoundRect(2 * Game.DIM.width / 120 + Game.DIM.width / 5, Game.DIM.height - Game.DIM.height / 15,
				Game.DIM.width / 5, Game.DIM.height / 120, Game.DIM.height / 240, Game.DIM.height / 240);

		// Ufo meter
		if (CommandCenter.getInstance().getUfoFloater() != null) {
			g.setColor(Color.red);
			g.fillRoundRect(3 * Game.DIM.width / 120 + 2 * Game.DIM.width / 5, Game.DIM.height - Game.DIM.height / 15,
					CommandCenter.getInstance().getUfoFloater().getExpire() * (Game.DIM.width / 5) / 333,
					Game.DIM.height / 120, Game.DIM.height / 240, Game.DIM.height / 240);
		}
		g.setColor(Color.white);
		g.drawRoundRect(3 * Game.DIM.width / 120 + 2 * Game.DIM.width / 5, Game.DIM.height - Game.DIM.height / 15,
				Game.DIM.width / 5, Game.DIM.height / 120, Game.DIM.height / 240, Game.DIM.height / 240);
	}
	
	private void initView() {
		Graphics g = getGraphics();			// get the graphics context for the panel
		g.setFont(fnt);						// take care of some simple font stuff
		fmt = g.getFontMetrics();
		nFontWidth = fmt.getMaxAdvance();
		nFontHeight = fmt.getHeight();
		g.setFont(fntBig);					// set font info
	}
	
	// This method draws some text to the middle of the screen before/after a game
	private void displayTextOnScreen() {

		grpOff.setFont(fntBig);
		fmt = grpOff.getFontMetrics();
		strDisplay = "ASTEROIDS";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6);

		grpOff.setFont(fnt);
		fmt = grpOff.getFontMetrics();
		strDisplay = "'S' to Start";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 80);

		strDisplay = "'P' to Pause";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 120);

		strDisplay = "'Q' to Quit";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 160);

		strDisplay = "arrow keys to turn and thrust";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 200);

		strDisplay = "space bar to fire";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 240);

		strDisplay = "'M' for music/mute";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 280);

		strDisplay = "left middle finger on 'E', hold for Steerable Missile, arrow keys to steer";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 320);

		strDisplay = "left index finger on 'F' for Cruise Missile";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 360);

		strDisplay = "left pinky on 'A' for Hyperspace";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 400);

		strDisplay = "red floater : avoid at all costs!";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 480);

		strDisplay = "yellow floater : explosive bullets";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 520);

		strDisplay = "green floater : new ship";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 560);

		strDisplay = "blue floater : shield";
		grpOff.drawString(strDisplay,
				(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 6
						+ nFontHeight + 600);

	}
	
	public GameFrame getFrm() {return this.gmf;}
	public void setFrm(GameFrame frm) {this.gmf = frm;}	
}