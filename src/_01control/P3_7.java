package _01control;

import java.util.Scanner;

public class P3_7 {

    public static void main(String[] args) {

        //Get income as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type income value: ");
        double income = scan.nextInt();

        double incomeTax = 0;

        //Calculate income tax for first tax bracket
        if (income <= 50_000) {
            incomeTax = 0.01 * income;
        }

        //Calculate income tax for second tax bracket
        else if (income <= 75_000) {
            incomeTax = 0.01 * 50_000 + 0.02 * (income - 50_000);
        }

        //Calculate income tax for third tax bracket
        else if (income <= 100_000) {
            incomeTax = 0.01 * 50_000 + 0.02 * 25_000 + 0.03 * (income - 75_000);
        }

        //Calculate income tax for fourth tax bracket
        else if (income <= 250_000) {
            incomeTax = 0.01 * 50_000 + 0.02 * 25_000 + 0.03 * 25_000 + 0.04 * (income - 100_000);
        }

        //Calculate income tax for fifth tax bracket
        else if (income <= 500_000) {
            incomeTax = 0.01 * 50_000 + 0.02 * 25_000 + 0.03 * 25_000 + 0.04 * 150_000 + 0.05 * (income - 250_000);
        }

        //Calculate income tax for sixth tax bracket
        else {
            incomeTax = 0.01 * 50_000 + 0.02 * 25_000 + 0.03 * 25_000 + 0.04 * 150_000 + 0.05 * 250_000 + 0.06 * (income - 500_000);
        }

        System.out.println(incomeTax);

    }

}
