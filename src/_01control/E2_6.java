package _01control;

import java.util.Scanner;

public class E2_6 {

    public static void main(String[] args) {

        //Define conversion factors
        double metersToMiles = 0.000621371192;
        double metersToFeet = 3.28084;
        double metersToInches = 39.37007874;

        //Get measurement in meters from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type measurement in meters: ");
        double measurementMeters = scan.nextFloat();

        //Calculate and print converted values
        System.out.println(measurementMeters * metersToMiles + " miles");
        System.out.println(measurementMeters * metersToFeet + " feet");
        System.out.println(measurementMeters * metersToInches + " inches");
    }

}
