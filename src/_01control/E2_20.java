package _01control;

public class E2_20 {

    public static void main(String[] args) {

        //Print triangular part of tree
        for (int i = 0; i < 4; i++) {

            //Print starting spaces
            for (int j = 0; j < 4 - i; j++) {
                System.out.print(" ");
            }

            //Print forward slope
            System.out.print("/");

            //Print spaces in between
            for (int j = 0; j < 2 * i; j++) {
                System.out.print(" ");
            }

            //Print backward slope
            System.out.println("\\");
        }

        //Print horizontal line
        System.out.print(" ");

        for (int i = 0; i < 8; i++) {
            System.out.print("-");
        }

        System.out.println("");

        //Print tree bark
        for (int i = 0; i < 2; i++) {

            System.out.print("   ");
            System.out.print("\"");
            System.out.print("  ");
            System.out.println("\"");

        }

    }

}
