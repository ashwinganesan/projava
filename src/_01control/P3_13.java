package _01control;

import java.util.Scanner;

public class P3_13 {

    public static void main(String[] args) {

        String romanYear = "";

        //Get year as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type year: ");
        String year = scan.nextLine();

        //Extract first digit of year
        int digit1 = Integer.parseInt(year.substring(0,1));

        //Add Ms for 1000s
        for (int i = 0; i < digit1; i++) {
            romanYear += "M";
        }

        //Extract second digit of year
        int digit2 = Integer.parseInt(year.substring(1,2));

        //Add Cs for 100s
        if (digit2 < 4) {
            for (int i = 0; i < digit2; i++) {
                romanYear += "C";
            }
        }

        //Add CD for 400
        else if (digit2 == 4) {
            romanYear += "CD";
        }

        //Add D and Cs for 500 - 800
        else if (digit2 < 9){
            romanYear += "D";
            for (int i = 0; i < digit2 - 5; i++) {
                romanYear += "C";
            }
        }

        //Add CM for 900
        else {
            romanYear += "CM";
        }

        //Extract third digit of year
        int digit3 = Integer.parseInt(year.substring(2,3));

        //Add Xs for 10s
        if (digit3 < 4) {
            for (int i = 0; i < digit3; i++) {
                romanYear += "X";
            }
        }

        //Add XL for 40
        else if (digit3 == 4) {
            romanYear += "XL";
        }

        //Add L and Xs for 50 - 80
        else if (digit3 < 9){
            romanYear += "L";
            for (int i = 0; i < digit3 - 5; i++) {
                romanYear += "X";
            }
        }

        //Add XC for 90
        else {
            romanYear += "XC";
        }

        //Extract fourth digit of year
        int digit4 = Integer.parseInt(year.substring(3,4));

        //Add Is for 1s
        if (digit4 < 4) {
            for (int i = 0; i < digit4; i++) {
                romanYear += "I";
            }
        }

        //Add IV for 4
        else if (digit4 == 4) {
            romanYear += "IV";
        }

        //Add V and Is for 5 - 8
        else if (digit4 < 9){
            romanYear += "V";
            for (int i = 0; i < digit4 - 5; i++) {
                romanYear += "I";
            }
        }

        //Add IX for 9
        else {
            romanYear += "IX";
        }

        System.out.println(romanYear);
    }

}


