package _01control;

import java.util.Scanner;

public class E4_17 {

    public static void main(String[] args) {

        //Get number as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type number: ");
        int x = scan.nextInt();

        //Initialize binary number as string
        String binary = "";

        //Add to string digit by digit
        while (x > 0) {
            binary = x % 2 + binary;
            x = x / 2;
        }

        System.out.println(binary);

    }

}
