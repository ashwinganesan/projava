package _01control;

import java.util.Scanner;

public class E2_4 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        //Get first integer from user
        System.out.print("Type first integer: ");
        int x = scan.nextInt();

        //Get second integer from user
        System.out.print("Type second integer: ");
        int y = scan.nextInt();

        //Calculate and print required values
        System.out.println("Sum: " + (x + y));
        System.out.println("Difference: " + (x - y));
        System.out.println("Product: " + (x * y));

        //Calculate average as decimal value if required
        if (Math.abs(x - y) % 2 == 1) {
            System.out.println("Average: " + (((float)x + (float)y) / 2));
        } else {
            System.out.println("Average: " + ((x + y) / 2));
        }

        System.out.println("Distance: " + Math.abs(x - y));
        System.out.println("Maximum: " + Math.max(x, y));
        System.out.println("Minimum: " + Math.min(x, y));
    }

}