package _01control;

import java.util.Scanner;

public class E3_14 {

    public static void main(String[] args) {

        String season = "";

        //Get month and day from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type month number: ");
        double month = scan.nextInt();
        System.out.print("Type day number: ");
        double day = scan.nextInt();

        //Set season according to month
        if (month >= 1 && month <= 3) {
            season = "Winter";
        }

        else if (month >= 4 && month <= 6) {
            season = "Spring";
        }

        else if (month >= 7 && month <= 9) {
            season = "Summer";
        }

        else if (month >= 10 && month <= 12) {
            season = "Fall";
        }

        //Choose correct season according to day
        if (month % 3 == 0 && day >= 21) {

            if (season == "Winter") {
                season = "Spring";
            }

            else if (season == "Spring") {
                season = "Summer";
            }

            else if (season == "Summer") {
                season = "Fall";
            }

            else {
                season = "Winter";
            }

        }

        System.out.print(season);

    }

}
