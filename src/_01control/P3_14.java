package _01control;

import java.util.Scanner;

public class P3_14 {

    public static void main(String[] args) {

        //Get year as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type year: ");
        int year = scan.nextInt();

        //Boolean to indicate leap year
        boolean isLeapYear = false;

        //Implement logic to check leap year
        if ((year <= 1582 && year % 4 == 0) || (year > 1582 && year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))) {
            isLeapYear = true;
        }

        System.out.println(isLeapYear);

    }

}
