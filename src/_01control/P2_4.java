package _01control;

import java.util.Scanner;

public class P2_4 {

    public static void main(String[] args) {

        //Get 10-digit number from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type 10-digit telephone number: ");
        String telNo = scan.nextLine();

        //Extract area code
        String areaCode = "(" + telNo.substring(0, 3) + ")";

        //Print required concatenated string
        System.out.println(areaCode + " " + telNo.substring(3, 6) + "-" + telNo.substring(6));
    }

}
