package _01control;

import java.util.Scanner;

public class E4_1 {

    public static void main(String[] args) {

        //Find sum of all even numbers between 2 and 100
        int sum1 = 0;

        for (int i = 2; i <= 100; i += 2) {
            sum1 += i;
        }

        System.out.println("a: " + sum1);

        //Find sum of all squares between 1 and 100
        int sum2 = 0;

        for (int i = 1; i <= 10; i++) {
            sum2 += Math.pow(i, 2);
        }

        System.out.println("b: " + sum2);

        //Print all powers of 2 from 2^0 to 2^20
        System.out.print("c: ");

        for (int i = 0; i <= 20; i++) {
            System.out.print((int)Math.pow(2, i) + " ");
        }

        System.out.println("");

        //Get number a as input from user
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Type first number: ");
        int a = scan1.nextInt();

        //Get number b as input from user
        System.out.print("Type second number: ");
        int b = scan1.nextInt();

        //Find sum of all odd integers between a and b
        int sum3 = 0;

        for (int i = a; i <= b; i++) {
            if (i % 2 == 1) {
                sum3 += i;
            }
        }

        System.out.println("d: " + sum3);

        //Get input x from user
        Scanner scan2 = new Scanner(System.in);
        System.out.print("Type number: ");
        String x = scan2.nextLine();

        //Find sum of all odd digits of x
        int sum4 = 0;
        int currentDigit = 0;

        for (int i = 0; i < x.length(); i++) {
            currentDigit = Character.getNumericValue(x.charAt(i));
            if (currentDigit % 2 == 1) {
                sum4 += currentDigit;
            }
        }

        System.out.println("e: " + sum4);

    }

}
