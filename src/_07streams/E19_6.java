package _07streams;

import java.util.Currency;

public class E19_6 {

    public static void main(String[] args) {

        Currency.getAvailableCurrencies().stream()
                                            .map(c -> c.getDisplayName()) // map to display name
                                            .sorted() // sort
                                            .forEach(c -> System.out.println(c)); //print

    }
}
