package _07streams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_5 {

    public static void main(String[] args) {

        // Initialize arraylist to store words
        ArrayList<String> words = new ArrayList<>();

        // Open input file
        File file = new File("src/_07streams/words.txt");
        Scanner input = null;

        try {
            input = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist");
        }

        // Add words from input file to arraylist
        while (input.hasNextLine()) {
            words.addAll(Arrays.asList(input.nextLine().split(" ")));
        }

        input.close();

        // Print output of toString method on words stream
        System.out.println(toString(words.stream(), 100));

    }

    // Method to turn stream into comma separated list of first n elements
    public static <T> String toString(Stream<T> stream, int n) {
        String output = stream.limit(n) // limit to n values
                            .map(t -> t.toString()) // convert to string
                            .collect(Collectors.joining(", ")); // join separated by commas
        return output;
    }

}
