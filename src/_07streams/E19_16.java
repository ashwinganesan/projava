package _07streams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class E19_16 {

    public static void main(String[] args) {

        // Initialize arraylist to store words
        ArrayList<String> words = new ArrayList<>();

        // Open input file
        File file = new File("src/_07streams/words.txt");
        Scanner input = null;

        try {
            input = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist");
        }

        // Add words from input file to arraylist
        while (input.hasNextLine()) {
            words.addAll(Arrays.asList(input.nextLine().split(" ")));
        }

        input.close();

        Map<String, Double> groupAverage = words.stream()
                                                .map(w -> w.toLowerCase()) // to lowercase
                                                .collect(Collectors.groupingBy(
                                                    w -> w.substring(0, 1), // group by first letter
                                                        Collectors.averagingInt(
                                                            w -> w.length()))); // find average

        // Print results
        for (Map.Entry<String, Double> entry : groupAverage.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

    }

}
