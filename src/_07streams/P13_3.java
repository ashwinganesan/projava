package _07streams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class P13_3 {

    // Initialize arraylist to store dictionary words
    public static ArrayList<String> words = new ArrayList();

    public static void main(String[] args) {

        // Open dictionary file
        File file = new File("src/_07streams/words.txt");
        Scanner input = null;

        try {
            input = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist");
        }

        // Add words from dictionary to arraylist
        while (input.hasNextLine()) {
            words.add(input.nextLine().toLowerCase());
        }

        input.close();

        // Initialize 2d char array for keypad
        char[][] keypad = {
                {'A', 'B', 'C'},
                {'D', 'E', 'F'},
                {'G', 'H', 'I'},
                {'J', 'K', 'L'},
                {'M', 'N', 'O'},
                {'P', 'Q', 'R', 'S'},
                {'T', 'U', 'V'},
                {'W', 'X', 'Y', 'Z'}
        };

        // Get phone number as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type phone number not containing 0 or 1: ");
        String numberString = scan.nextLine();

        // Initialize arraylist for phone number
        ArrayList<Integer> number = new ArrayList<>();

        // Store phone number in array list excluding special symbols
        for (int i = 0; i < numberString.length(); i++) {
            if (Character.isDigit(numberString.charAt(i))) {
                number.add(Character.getNumericValue(numberString.charAt(i)));
            }
        }

        // Call recursive method to find all possible strings
        findCombinationsRecursive(keypad, number.size() - 1, number, "");

    }

    // Recursive method to find all possible strings from phone number
    public static void findCombinationsRecursive(char[][] keypad, int index, ArrayList<Integer> number, String string) {

        // Base case: end of number reached
        if (index == -1) {

            // Call recursive method to print valid combinations of words
            printWordsRecursive(string,  "");
            return;
        }

        // Iterate over letters corresponding to number at current index
        for (char letter : keypad[number.get(index) - 2]) {

            // Call recursive method on previous number with letter corresponding to current number added to string
            findCombinationsRecursive(keypad, index - 1, number, letter + string);
        }

    }

    // Recursive method to print all valid combinations of words in string
    public static void printWordsRecursive(String string, String output) {

        // Base case: end of string reached, print output string
        if (string.length() == 0) {
            System.out.println(output);
            return;
        }

        // Initialize boolean to track if word found
        boolean wordFound = false;

        // Iterate over all letters in string
        for (int i = string.length() - 1; i >= 0; i--) {

            // If substring to end of string forms word
            if (words.contains(string.substring(i).toLowerCase())) {
                wordFound = true;
                // Call recursive method to print words on substring with found word excluded, add found word to output string
                printWordsRecursive(string.substring(0, i), string.substring(i) + " " + output);
            }
        }

        // Return if word not found
        if (!wordFound) {
            return;
        }
    }

}
