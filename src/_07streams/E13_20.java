package _07streams;

public class E13_20 {

    public static void main(String[] args) {

        // Initialize denominations array
        int[] denominations = {100, 20, 5, 1};

        // Initialize starting combination array to input into recursive method
        int[] combination = {0, 0, 0, 0};

        // Initialize starting index in denominations array to 0
        int startDenomination = 0;

        // Print all combinations for test price
        printCombinationsRecursive(denominations, combination, startDenomination, 112);

    }

    // Recursive method to print all combinations for given price
    public static void printCombinationsRecursive(int[] denominations, int[] combination, int index, int price){

        // Base case: price = 0
        if (price == 0) {
            // Print combination
            System.out.println(combination[0] + ", " + combination[1] + ", " + combination[2] + ", " + combination[3]);
            return;
        }
        
        // Base case: end of denominations array reached
        if (index == (denominations.length)) {
            return;
        }

        // Set denomination according to current index
        int denomination = denominations[index];

        // Iterate over all possible quantities of current denomination
        for (int i = 0; i <= (price / denomination); i++) {

            // Set quantity of current denomination
            combination[index] = i;

            // Call recursive method for next lower denomination with price decreased by total value of current denomination used
            printCombinationsRecursive(denominations, combination,index + 1, price - (i * denomination));

        }
    }

}
