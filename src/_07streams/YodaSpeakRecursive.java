package _07streams;

import java.util.Scanner;

public class YodaSpeakRecursive {

    public static void main(String[] args) {

        // Get sentence as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type sentence: ");
        String sentence = scan.nextLine();

        // Call recursive method to reverse words and print result
        System.out.println(yodaSpeakRecursive(sentence));

    }

    // Recursive method to reverse words in sentence
    public static String yodaSpeakRecursive(String sentence) {

        // Return word if sentence only consists of one word
        if (sentence.split(" ").length == 1) {
            return sentence;
        }

        // Split sentence into words
        String[] words = sentence.split(" ");

        // Add last word to output and call recursive method on remaining sentence
        return words[words.length - 1] + " " + yodaSpeakRecursive(sentence.substring(0, sentence.lastIndexOf(" ")));

    }

}
