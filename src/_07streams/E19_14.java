package _07streams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class E19_14 {

    public static void main(String[] args) {

        // Initialize arraylist to store words
        ArrayList<String> words = new ArrayList<>();

        // Open input file
        File file = new File("src/_07streams/warandpeace.txt");
        Scanner input = null;

        try {
            input = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist");
        }

        // Add words from input file to arraylist
        while (input.hasNextLine()) {
            words.addAll(Arrays.asList(input.nextLine().split(" ")));
        }

        input.close();

        // If the program is run multiple times, the order in which the words are printed is different every time due to parallelization.
        words.stream()
                .parallel() // parallelize
                .filter(s -> s.equals(new StringBuilder(s).reverse().toString())) // filter palindromes
                .filter(s -> s.length() >= 5) // filter according to length
                .forEach(s -> System.out.println(s)); // print

    }

}
