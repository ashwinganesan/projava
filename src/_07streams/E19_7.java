package _07streams;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Function;

public class E19_7 {

    public static void main(String[] args) {

        // Function to turn string into first character, three periods and last character
        Function<String, String> myFunction = s -> s.charAt(0) + "..." + s.charAt(s.length() - 1);

        // Initialize arraylist to store input words
        ArrayList<String> words = new ArrayList<>();

        // Get words as input from user
        String word = "";
        Scanner scan = new Scanner(System.in);
        while (!word.equals("q")) {
            System.out.print("Type word, q to quit: ");
            word = scan.nextLine();
            // Add word to list if length is at least 2
            if (word.length() >= 2) {
                words.add(word);
            }
        }

        words.stream()
                .map(myFunction) // map using function
                .forEach(s -> System.out.println(s)); // print
    }

}
