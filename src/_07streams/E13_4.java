package _07streams;

public class E13_4 {

    public static void main(String[] args) {

        // Print binary conversion of test value
        System.out.println(binaryRecursive(15));

    }

    // Recursive method to convert decimal value to binary String
    public static String binaryRecursive(int n) {
        if (n == 0) {
            // Base case: n = 0
            return "";
        }
        else {
            // Binary conversion of n = binary conversion of floor(n/2) with 0 or 1 added at end depending on whether n is even or odd
            return binaryRecursive(n / 2) + Integer.toString(n % 2);
        }
    }

}
