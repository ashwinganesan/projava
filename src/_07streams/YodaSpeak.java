package _07streams;

import java.util.Scanner;

public class YodaSpeak {

    public static void main(String[] args) {

        // Get sentence as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type sentence: ");
        String sentence = scan.nextLine();

        // Split sentence into words and initialize output string
        String[] words = sentence.split(" ");
        String output = "";

        // Iterate over words backwards and add to output string
        for (int i = words.length - 1; i >= 0; i--) {
            output += words[i] + " ";
        }

        // Print output string
        System.out.println(output);

    }

}
