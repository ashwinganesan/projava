package _02arrays;

import java.util.Arrays;
import java.util.Random;

public class E6_12 {

    public static void main(String[] args) {

        //Initialize int array with 20 elements
        int[] twentyRandom = new int[20];
        Random ran = new Random();

        //Fill array with 20 random integers from 0 to 99 and print array
        for (int i = 0; i < twentyRandom.length; i++) {
            twentyRandom[i] = ran.nextInt(100);
            System.out.print(twentyRandom[i] + " ");
        }
        System.out.println();

        //Sort array
        Arrays.sort(twentyRandom);

        //Print sorted array
        for (int i = 0; i < twentyRandom.length; i++) {
            System.out.print(twentyRandom[i] + " ");
        }

    }

}
