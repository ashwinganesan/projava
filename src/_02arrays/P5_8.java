package _02arrays;

public class P5_8 {

    //Method to check whether input year is leap year
    public static boolean isLeapYear(int year) {

        if (year % 4 == 0) {

            //Return true if year is divisible by 4 before Gregorian correction
            if (year <= 1582) {
                return true;
            }

            //After Gregorian correction
            else {
                if (year % 100 == 0) {
                    //Return true if year is divisible by 400
                    if (year % 400 == 0) {
                        return true;
                    }
                    //Return false if year is divisible by 100 but not by 400
                    else {
                        return false;
                    }
                }
                //Return true if year is divisible by 4 but not by 100
                else {
                    return true;
                }
            }

        }

        //Return false if year is not divisible by 4
        else {
            return false;
        }

    }

}
