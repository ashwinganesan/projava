//https//stackoverflow.com/questions/15738918/splitting-a-csv-file-with-quotes-as-text-delimiter-using-string-split

package _02arrays;

import java.io.*;
import java.util.*;

public class P7_5 {

    //Initialize arraylist of arraylists to store values from CSV
    public static ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();

    //Initialize counter for number of rows in CSV
    public static int rowCounter = 0;

    public static void main(String[] args) throws FileNotFoundException {

        //Get input file name from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type input file name: ");
        String inputFileName = scan.nextLine();

        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);

        //Read input file line by line
        while (in.hasNextLine()) {

            //Add arraylist to table corresponding to current line
            table.add(new ArrayList<String>());
            //Split line based on commas with even number of quotation marks until end. Regex found at
            //https://stackoverflow.com/questions/15738918/splitting-a-csv-file-with-quotes-as-text-delimiter-using-string-split
            String[] inputLineSplit = in.nextLine().split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

            for (int i = 0; i < inputLineSplit.length; i++) {
                //Trim trailing white spaces
                inputLineSplit[i] = inputLineSplit[i].trim();
                //If string is enclosed by quotation marks, remove enclosing quotation marks and replace double quotation marks by single ones
                if (inputLineSplit[i].charAt(0) == '"' && inputLineSplit[i].charAt(inputLineSplit[i].length() - 1) == '"') {
                    inputLineSplit[i] = inputLineSplit[i].substring(1, inputLineSplit[i].length() - 1);
                    inputLineSplit[i] = inputLineSplit[i].replaceAll("\"\"", "\"");
                }
                //Add string to arraylist corresponding to current line
                table.get(rowCounter).add(inputLineSplit[i]);
            }

            rowCounter++;

        }

    }

    //Method to return number of rows
    public static int numberOfRows() {
        return rowCounter;
    }

    //Method to return number of fields
    public static int numberOfFields() {
        return rowCounter * table.get(0).size();
    }

    //Method to return contents of field at (row, column) with indexing starting from 1
    public static String field(int row, int column) {
        return table.get(row - 1).get(column - 1);
    }

}
