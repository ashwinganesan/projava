package _02arrays;

import java.util.Scanner;

public class P5_24 {

    public static void main(String[] args) {

        //Initialize total as integer
        int total = 0;

        //Get roman numeral as string from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type year in roman numerals: ");
        String romanNumeral = scan.nextLine();

        //Continue iteration while string is not empty
        while (romanNumeral.length() > 0) {
            //Add numeric value of first letter to total if its value is at least equal to that of the second letter
            if (romanNumeral.length() == 1 || letterToNumeric(romanNumeral.charAt(0)) >= letterToNumeric(romanNumeral.charAt(1))) {
                total += letterToNumeric(romanNumeral.charAt(0));
                //Remove first letter from string
                romanNumeral = romanNumeral.substring(1);
            }
            // Else add combined value of first two letters to total
            else {
                int difference = letterToNumeric(romanNumeral.charAt(1)) - letterToNumeric(romanNumeral.charAt(0));
                total += difference;
                //Remove first two letters from string
                romanNumeral = romanNumeral.substring(2);
            }
        }

        //Print converted decimal value
        System.out.println("Year in decimal numbers: " + total);

    }

    //Method to return numeric value of each letter
    public static int letterToNumeric(char letter) {
        int numericValue = 0;
        switch (letter) {
            case 'I':
                numericValue = 1;
                break;
            case 'V':
                numericValue = 5;
                break;
            case 'X':
                numericValue = 10;
                break;
            case 'L':
                numericValue = 50;
                break;
            case 'C':
                numericValue = 100;
                break;
            case 'D':
                numericValue = 500;
                break;
            case 'M':
                numericValue = 1000;
                break;
        }
        return numericValue;
    }

}
