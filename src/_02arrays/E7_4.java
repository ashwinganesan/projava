package _02arrays;

import java.io.*;
import java.util.Scanner;

public class E7_4 {

    public static void main(String[] args) throws FileNotFoundException {

        //Get input and output file names from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type input file name: ");
        String inputFileName = scan.nextLine();
        System.out.print("Type output file name: ");
        String outputFileName = scan.nextLine();

        //Initialize File, Scanner and PrintWriter objects and line counter for file
        File inputFile = new File(inputFileName);
        Scanner in = new Scanner(inputFile);
        PrintWriter out = new PrintWriter(outputFileName);
        int lineCounter = 1;

        //Write from input file to output file line by line including counter and delimiters
        while (in.hasNextLine()) {
            out.println("/* " + lineCounter + " */ " + in.nextLine());
            lineCounter++;
        }

        in.close();
        out.close();

    }

}
