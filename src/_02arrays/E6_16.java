package _02arrays;

import java.util.*;

public class E6_16 {

    public static void main(String[] args) {

        //Initialize array list to store inputs
        ArrayList<Double> inputs = new ArrayList<Double>();

        //Get inputs from user while inputs are numeric
        Scanner scan = new Scanner(System.in);
        System.out.print("Type value, non-numeric to quit: ");
        while (scan.hasNextDouble()) {
            inputs.add(scan.nextDouble());
            System.out.print("Type value: ");
        }

        //Find maximum out of inputs
        double maxInput = Collections.max(inputs);

        //Initialize array to store histogram
        char[][] histogram = new char[20][inputs.size()];

        //Fill array with blank spaces
        for (int i = 0; i < histogram.length; i++) {
            for (int j = 0; j < histogram[0].length; j++) {
                histogram[i][j] = ' ';
            }
        }

        //Fill array with asterisks as required
        for (int j = 0; j < inputs.size(); j++) {
            //Height of line of asterisks corresponding to input j
            int height = numAsterisks(inputs.get(j), maxInput);
            for (int i = 0; i < height; i++) {
                histogram[histogram.length - 1 - i][j] = '*';
            }
        }

        //Print histogram
        for (int i = 0; i < histogram.length; i++) {
            for (int j = 0; j < histogram[0].length; j++) {
                System.out.print(histogram[i][j]);
            }
            System.out.println();
        }

    }

    //Method to find number of asterisks to be printed for a certain input and maximum input
    public static int numAsterisks(double input, double maxInput) {
        return (int)(20 * input / maxInput);
    }

}
