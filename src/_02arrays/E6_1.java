package _02arrays;

import java.util.Random;

public class E6_1 {

    public static void main(String[] args) {

        //Initialize int array with 10 elements
        int[] tenRandom = new int[10];
        Random ran = new Random();

        //Fill array with 10 random integers
        for (int i = 0; i < tenRandom.length; i++) {
            tenRandom[i] = ran.nextInt();
        }

        //Print every element at an even index
        for (int i = 0; i < tenRandom.length; i += 2) {
            System.out.print(tenRandom[i] + " ");
        }
        System.out.println();

        //Print every even element
        for (int i = 0; i < tenRandom.length; i++) {
            if (tenRandom[i] % 2 == 0) {
                System.out.print(tenRandom[i] + " ");
            }
        }
        System.out.println();

        //Print all elements in reverse order
        for (int i = tenRandom.length - 1; i >= 0; i--) {
            System.out.print(tenRandom[i] + " ");
        }
        System.out.println();

        //Print first and last element
        System.out.println(tenRandom[0] + " " + tenRandom[tenRandom.length - 1]);

    }
}
