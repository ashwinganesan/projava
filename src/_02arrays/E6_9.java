package _02arrays;

public class E6_9 {

    //Method to check whether input arrays have same elements in same order
    public static boolean equals(int[] a, int[] b) {

        //Initialize boolean variable to check equality
        boolean isEqual = false;

        //Change boolean to true if arrays have same length and same elements in each position
        if (a.length == b.length) {
            isEqual = true;
            for (int i = 0; i < a.length; i++) {
                if (a[i] != b[i]) {
                    isEqual = false;
                }
            }
        }

        //Return boolean
        return isEqual;

    }

}
