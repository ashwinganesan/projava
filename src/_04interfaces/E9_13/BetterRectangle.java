package _04interfaces.E9_13;

import java.awt.*;

public class BetterRectangle extends Rectangle {

    // Constructor
    public BetterRectangle(int width, int height, int x, int y){
        super.setSize(width, height);
        super.setLocation(x, y);
    }

    // Return area of rectangle
    public int getArea(){
        return width * height;
    }

    // Return perimeter of rectangle
    public int getPerimeter(){
        return 2 * (width + height);
    }

}
