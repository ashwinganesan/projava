package _04interfaces.E9_13;


public class Driver {

    public static void main(String[] args) {

        // Create new BetterRectangle
        BetterRectangle chad = new BetterRectangle(40, 20, 50, 50);

        // Print area and perimeter
        System.out.println(chad.getArea());
        System.out.println(chad.getPerimeter());

    }

}
