package _04interfaces.P9_6;

import java.time.LocalDate;

public class Onetime extends Appointment {

    // Constructor
    public Onetime(String description, LocalDate date){
        super.setDescription(description);
        super.setDate(date);
    }

    // Return true if appointment occurs on date
    public boolean occursOn(int year, int month, int day){
        return year == super.getDate().getYear() && month == super.getDate().getMonthValue() && day == super.getDate().getDayOfMonth();
    }

    // toString method
    public String toString(){
        return "Onetime Appointment: " + super.getDescription();
    }

}
