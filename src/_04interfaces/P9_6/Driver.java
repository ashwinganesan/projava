package _04interfaces.P9_6;

import java.time.LocalDate;
import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {

        // Initialize appointments array
        Appointment[] appointments = new Appointment[10];

        // Fill array with mixture of appointments
        appointments[0] = new Daily("Walk the dog", LocalDate.of(2021, 1, 1));
        appointments[1] = new Onetime("Dental appointment", LocalDate.of(2021, 4, 18));
        appointments[2] = new Monthly("Take a shower", LocalDate.of(2020, 9, 7));
        appointments[3] = new Monthly("Visit the barber", LocalDate.of(2021, 7, 22));
        appointments[4] = new Daily("Brush teeth", LocalDate.of(2021, 3, 4));
        appointments[5] = new Onetime("Go scuba diving", LocalDate.of(2020, 11, 16));
        appointments[6] = new Monthly("Visit parents", LocalDate.of(2021, 2, 15));
        appointments[7] = new Onetime("Carwash", LocalDate.of(2020, 10, 28));
        appointments[8] = new Daily("Have dinner", LocalDate.of(2021, 6, 10));
        appointments[9] = new Monthly("Buy groceries", LocalDate.of(2021, 12, 25));

        // Get day, month and year as input from user
        Scanner scan = new Scanner(System.in);
        System.out.print("Type day of month: ");
        int day = scan.nextInt();
        System.out.print("Type month from 1 to 12: ");
        int month = scan.nextInt();
        System.out.print("Type year: ");
        int year = scan.nextInt();

        // Print appointments scheduled
        System.out.println("Appointments scheduled for " + day + "/" + month + "/" + year + ":");
        for(Appointment appointment : appointments){
            if(appointment.occursOn(year, month, day)){
                System.out.println(appointment.toString());
            }
        }

    }

}
