package _04interfaces.P9_6;

import java.time.LocalDate;

public abstract class Appointment {

    // Initialize variables for description and date
    private String description;
    private LocalDate date;

    // Get description
    public String getDescription() {
        return description;
    }

    // Get date
    public LocalDate getDate() {
        return date;
    }

    // Set description
    public void setDescription(String description) {
        this.description = description;
    }

    // Set date
    public void setDate(LocalDate date) {
        this.date = date;
    }

    // Define abstract method occursOn
    public abstract boolean occursOn(int year, int month, int day);

    // Define abstract method toString
    public abstract String toString();

}
