package _04interfaces.P9_6;

import java.time.LocalDate;

public class Monthly extends Appointment {

    // Constructor
    public Monthly(String description, LocalDate date){
        super.setDescription(description);
        super.setDate(date);
    }

    // Return true if appointment occurs on day of month
    public boolean occursOn(int year, int month, int day){
        return day == super.getDate().getDayOfMonth();
    }

    // toString method
    public String toString(){
        return "Monthly Appointment: " + super.getDescription();
    }

}
