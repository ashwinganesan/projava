package _04interfaces.P9_6;

import java.time.LocalDate;

public class Daily extends Appointment {

    // Constructor
    public Daily(String description, LocalDate date){
        super.setDescription(description);
        super.setDate(date);
    }

    // Return true
    public boolean occursOn(int year, int month, int day){
        return true;
    }

    // toString method
    public String toString(){
        return "Daily Appointment: " + super.getDescription();
    }

}
