package _04interfaces.E9_8;

public class Driver {

    public static void main(String[] args) {

        // Create new basic account
        BankAccount myAccount = new BasicAccount();

        // Deposit 5000
        myAccount.deposit(5000);

        // Print balance
        System.out.println(myAccount.getBalance());

        // Attempt to withdraw 7000
        myAccount.withdraw(7000);

        // Print balance
        System.out.println(myAccount.getBalance());

        // Withdraw 3000
        myAccount.withdraw(3000);

        // Print balance
        System.out.println(myAccount.getBalance());

    }

}
