package _04interfaces.E9_8;

public class BankAccount {

    // Initialize balance variable
    private double balance;

    // Constructor
    public BankAccount(){
        balance = 0;
    }

    // Deposit amount into account
    public void deposit(double amount){
        balance = balance + amount;
    }

    // Withdraw amount from account
    public void withdraw(double amount){
        balance = balance - amount;
    }

    // Month end processing
    public void monthEnd(){
    }

    // Return current balance
    public double getBalance(){
        return balance;
    }
}
