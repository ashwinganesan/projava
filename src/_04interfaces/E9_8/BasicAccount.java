package _04interfaces.E9_8;

public class BasicAccount extends BankAccount{

    // Withdraw amount from account only if it does not exceed balance
    @Override
    public void withdraw(double amount){

        // Get current balance using superclass method
        double balance = this.getBalance();

        // Print error message to console if amount exceeds balance
        if(amount > balance){
            System.out.println("Withdrawal amount exceeded balance. Please try again.");
        }

        // Withdraw amount otherwise
        else{
            super.withdraw(amount);
        }

    }

}
