package _04interfaces.E9_17;

public class SodaCan implements Measurable {

    // Initialize height and radius
    private double height;
    private double radius;

    // Constructor
    public SodaCan(double height, double radius) {
        this.height = height;
        this.radius = radius;
    }

    // Return surface area of can
    public double getSurfaceArea(){
        return 2 * Math.PI * radius * (radius + height);
    }

    // Return volume of can
    public double getVolume(){
        return Math.PI * Math.pow(radius, 2) * height;
    }

    // Return surface area of can as measure
    public double getMeasure(){
        return this.getSurfaceArea();
    }

}
