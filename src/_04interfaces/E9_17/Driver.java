package _04interfaces.E9_17;

public class Driver {

    public static void main(String[] args) {

        // Initialize sum variable to add areas
        double sum = 0;

        // Initialize number of cans
        int nCans = 10;

        // Initialize Measurable array
        Measurable[] sodaCans = new Measurable[nCans];

        // Fill array with SodaCan objects having random heights between 0 and 10 and widths between 0 and 4, add surface area to sum
        for(Measurable sodaCan : sodaCans){
            sodaCan = new SodaCan(10 * Math.random(), 4 * Math.random());
            sum += sodaCan.getMeasure();
        }

        // Calculate and print average of surface areas
        System.out.println("Average surface area: " + sum / nCans);

    }

}
