package _04interfaces.E9_17;

public interface Measurable {

    // Return measure of object
    double getMeasure();

}
