package _04interfaces.E9_11;

public class Student extends Person {

    // Initialize variable for major
    private String major;

    // Constructor
    public Student(String name, int yearOfBirth, String major) {
        super(name, yearOfBirth);
        this.major = major;
    }

    // toString method
    public String toString() {
        return(super.toString() + "[major=" + major + "]");
    }

}
