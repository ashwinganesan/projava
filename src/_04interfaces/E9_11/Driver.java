package _04interfaces.E9_11;

public class Driver {

    public static void main(String[] args) {

        // Instantiate person, student and instructor objects
        Person person = new Person("Joe", 1984);
        Person student = new Student("Jessica", 1993, "Anthropology");
        Person instructor = new Instructor("James", 1971, 40000);

        // Call toString method on each object and print
        System.out.println(person.toString());
        System.out.println(student.toString());
        System.out.println(instructor.toString());

    }

}
