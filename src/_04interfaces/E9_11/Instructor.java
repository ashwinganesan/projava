package _04interfaces.E9_11;

public class Instructor extends Person {

    // Initialize variable for salary
    private int salary;

    // Constructor
    public Instructor(String name, int yearOfBirth, int salary) {
        super(name, yearOfBirth);
        this.salary = salary;
    }

    // toString method
    public String toString() {
        return(super.toString() + "[salary=" + salary + "]");
    }

}
