package _04interfaces.E9_11;

public class Person {

    // Initialize variables for name and year of birth
    private String name;
    private int yearOfBirth;

    // Constructor
    public Person(String name, int yearOfBirth) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    // toString method
    public String toString() {
        return("Person[name=" + name + "][year=" + yearOfBirth + "]");
    }

}
