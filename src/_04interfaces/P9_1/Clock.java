package _04interfaces.P9_1;

import java.time.LocalTime;

public class Clock {

    // Initialize string variable for time
    private String timeString;

    // Constructor
    public Clock(){
        this.timeString = LocalTime.now().toString();
    }

    // Return string segment representing hours
    public String getHours(){
        return timeString.split(":")[0];
    }

    // Return string segment representing minutes
    public String getMinutes(){
        return timeString.split(":")[1];
    }

    // Return time consisting of hours and minutes
    public String getTime(){
        return getHours() + ":" + getMinutes();
    }

}
