package _04interfaces.P9_1;

public class WorldClock extends Clock {

    // Initialize offset hours and minutes
    private int offsetHours;
    private int offsetMinutes;

    // Default constructor
    public WorldClock(){
        super();
    }

    // Constructor with one argument offsetHours
    public WorldClock(int offsetHours){
        super();
        this.offsetHours = offsetHours;
        this.offsetMinutes = 0;
    }

    // Constructor with two arguments offsetHours and offsetMinutes
    public WorldClock(int offsetHours, int offsetMinutes){
        super();
        this.offsetHours = offsetHours;
        this.offsetMinutes = offsetMinutes;
    }

    // Return string segment representing hours
    @Override
    public String getHours(){

        // Add offset hours and account for overflow
        String hoursString = Integer.toString((Integer.parseInt(super.getHours()) + offsetHours) % 24);

        // Add preceding 0 to hours if single-digit
        if (hoursString.length() == 1){
            hoursString = "0" + hoursString;
        }

        return hoursString;

    }

    // Return string segment representing minutes
    @Override
    public String getMinutes(){

        // Add offset minutes and account for overflow
        String minutesString = Integer.toString((Integer.parseInt(super.getMinutes()) + offsetMinutes) % 60);

        // Add preceding 0 to minutes if single-digit
        if (minutesString.length() == 1){
            minutesString = "0" + minutesString;
        }

        return minutesString;

    }

}
