package _04interfaces.P9_1;

public class Driver {

    public static void main(String[] args) {

        // Create new clock set at CT and print time, hours and minutes
        Clock ct = new Clock();
        System.out.println(ct.getTime());
        System.out.println(ct.getHours());
        System.out.println(ct.getMinutes());

        // Create new world clock set at CT and print time
        ct = new WorldClock();
        System.out.println(ct.getTime());

        // Create new world clock set at EST and print time
        Clock est = new WorldClock(1);
        System.out.println(est.getTime());

        // Create new world clock set at GMT and print time
        Clock gmt = new WorldClock(6);
        System.out.println(gmt.getTime());

        // Create new world clock set at NPT and print time
        Clock npt = new WorldClock(11, 45);
        System.out.println(npt.getTime());
    }

}
