package _03objects.P8_5;

public class SodaCan {

    // Initialize height and radius
    private double height;
    private double radius;

    // Constructor
    public SodaCan(double height, double radius) {
        this.height = height;
        this.radius = radius;
    }

    // Return surface area of can
    public double getSurfaceArea(){
        return 2 * Math.PI * radius * (radius + height);
    }

    // Return volume of can
    public double getVolume(){
        return Math.PI * Math.pow(radius, 2) * height;
    }

}
