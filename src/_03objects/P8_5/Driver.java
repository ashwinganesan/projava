package _03objects.P8_5;

public class Driver {

    public static void main(String[] args) {

        // Create new SodaCan object
        SodaCan cocaColaCan = new SodaCan(12.2, 3.25);

        // Calculate surface area and volume and print to console
        System.out.println("A Coca Cola can has a surface area of " + cocaColaCan.getSurfaceArea() +
                            " cm^2 and a volume of " + cocaColaCan.getVolume() + " cm^3.");

    }

}
