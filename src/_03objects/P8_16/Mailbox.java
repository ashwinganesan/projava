package _03objects.P8_16;

import java.util.ArrayList;

public class Mailbox {

    // Initialize arraylist to store messages
    ArrayList<Message> messages = new ArrayList<>();

    // Add message to mailbox
    public void addMessage(Message m){
        messages.add(m);
    }

    // Get message from mailbox, throw exception if index invalid
    public Message getMessage(int i){
        if(i < messages.size()){
            return messages.get(i);
        }
        else{
            throw new IndexOutOfBoundsException("Invalid input");
        }
    }

    // Remove message from mailbox, throw exception if index invalid
    public void removeMessage(int i){
        if(i < messages.size()){
            messages.remove(i);
        }
        else{
            throw new IndexOutOfBoundsException("Invalid input");
        }
    }

}
