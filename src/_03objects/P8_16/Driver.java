package _03objects.P8_16;

public class Driver {

    public static void main(String[] args) {

        // Create first message object
        Message message1 = new Message("Wayne Rooney", "Rio Ferdinand");
        message1.append("Hi rio do u want picking up in the morning pal");


        // Create second message object
        Message message2 = new Message("Tyrion Lannister", "Cersei Lannister");
        message2.append("I will hurt you for this.");
        message2.append("I don't know how yet, but give me time.");
        message2.append("A day will come when you think yourself safe and happy,");
        message2.append("and suddenly your joy will turn to ashes in your mouth,");
        message2.append("and you'll know the debt is paid.");

        // Create third message object
        Message message3 = new Message("Fernando Alonso", "Nico Rosberg");
        message3.append("ALL DA TIME YOU HAVE TO LEAVE A SPACE");

        // Create new mailbox object
        Mailbox mailbox = new Mailbox();

        // Add messages to mailbox
        mailbox.addMessage(message1);
        mailbox.addMessage(message2);
        mailbox.addMessage(message3);

        // Print messages from mailbox
        System.out.println(mailbox.getMessage(0));
        System.out.println(mailbox.getMessage(1));
        System.out.println(mailbox.getMessage(2));

        // Remove third message from mailbox
        mailbox.removeMessage(2);

        // Attempt to print third message from mailbox
        // System.out.println(mailbox.getMessage(2));

    }

}
