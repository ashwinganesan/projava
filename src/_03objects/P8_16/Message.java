package _03objects.P8_16;

public class Message {

    // Initialize sender, recipient and message Strings
    private String sender;
    private String recipient;
    private String message;

    // Constructor
    public Message(String sender, String recipient){
        this.sender = sender;
        this.recipient = recipient;
        this.message = "";
    }

    // Append line to message
    public void append(String line){
        message = message + line + "%n";
    }

    // ToString method
    public String toString(){
        return "From: " + sender + "%n" + "To: " + recipient + "%n" + message;
    }

}
