package _03objects.P8_19;

import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {

        // Initialize starting angle and initial velocity
        double startingAngle = 0;
        double initialVelocity = 0;

        Scanner scan = new Scanner(System.in);

        // Get starting angle in radians as input from user

        System.out.print("Enter starting angle: ");
        startingAngle = scan.nextDouble();

        // Get initial velocity as input from user
        System.out.print("Enter initial velocity: ");
        initialVelocity = scan.nextDouble();

        // Initialize new cannonball object
        Cannonball angryBird = new Cannonball(0);

        // Shoot cannonball
        angryBird.shoot(startingAngle, initialVelocity);

    }

}
