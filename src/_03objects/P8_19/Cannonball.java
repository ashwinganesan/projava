package _03objects.P8_19;

public class Cannonball {

    // Initialize variables for x and y positions and velocities and constant for gravitational acceleration
    private double xPosition;
    private double yPosition;
    private double xVelocity;
    private double yVelocity;
    private static final double GRAV_ACC = -9.81;

    // Constructor
    public Cannonball(double xPosition){
        this.xPosition = xPosition;
        this.yPosition = 0;
    }

    // Change x and y positions and y velocity depending on movement time
    public void move(double sec){
        xPosition += xVelocity * sec;
        yPosition += yVelocity * sec;
        yVelocity += GRAV_ACC * sec;
    }

    // Get x position
    public double getX(){
        return xPosition;
    }

    // Get y position
    public double getY(){
        return yPosition;
    }

    // Simulate shooting cannonball
    public void shoot(double angle, double v){
        // Convert initial velocity into x and y components
        xVelocity = v * Math.cos(angle);
        yVelocity = v * Math.sin(angle);
        // While y position is positive
        do{
            // Move for 0.1 s
            move(0.1);

            // Set y position to 0 once it turns negative
            if(yPosition < 0){
                yPosition = 0;
            }

            // Output x and y position to console
            System.out.print("X position: " + this.getX());
            System.out.println(", Y position: " + this.getY());
        }
        while(yPosition > 0);
    }
}
