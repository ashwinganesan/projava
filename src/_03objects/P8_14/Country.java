package _03objects.P8_14;

public class Country {

    // Initialize variables for country name, population and area
    private String name;
    private int population;
    private int area;

    // Constructor
    public Country(String name) {
        this.name = name;
    }

    // Set population
    public void setPopulation(int population) {
        this.population = population;
    }

    // Set area
    public void setArea(int area) {
        this.area = area;
    }

    // Get country name
    public String getName() {
        return name;
    }

    // Get population
    public int getPopulation() {
        return population;
    }

    // Get area
    public int getArea() {
        return area;
    }

}
