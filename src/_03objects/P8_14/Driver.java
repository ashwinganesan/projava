package _03objects.P8_14;

import java.util.ArrayList;
import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {

        // Initialize arraylist of countries
        ArrayList<Country> countries = new ArrayList<>();

        // Take countries as input from user
        while(true){

            Scanner scan = new Scanner(System.in);

            // Get name of country as input from user
            System.out.print("Enter country name, q to quit: ");
            String input = scan.nextLine();

            // End loop if user chooses to quit
            if(input.equals("q")){
                break;
            }

            else{

                // Create new country object
                Country newCountry = new Country(input);

                // Get population of country as input from user
                System.out.print("Enter population: ");

                // Set population if user inputs integer, else end loop
                if(scan.hasNextInt()){
                    newCountry.setPopulation(scan.nextInt());
                }
                else{
                    System.out.println("Invalid population");
                    break;
                }

                // Get area of country as input from user
                System.out.print("Enter area: ");

                // Set area if user inputs integer, else end loop
                if(scan.hasNextInt()){
                    newCountry.setArea(scan.nextInt());
                }
                else{
                    System.out.println("Invalid area");
                    break;
                }

                // Add country to arraylist
                countries.add(newCountry);
            }
        }

        // Notify if no countries entered by user
        if (countries.size() == 0){
            System.out.println("No countries entered.");
        }

        // Iterate through arraylist to find countries with largest area, population and population density
        else{
            Country maxArea = countries.get(0);
            Country maxPopulation = countries.get(0);
            Country maxPopulationDensity = countries.get(0);

            for(int i = 1; i < countries.size(); i++){
                if(countries.get(i).getPopulation() > maxArea.getPopulation()){
                    maxPopulation = countries.get(i);
                }
                if(countries.get(i).getArea() > maxArea.getArea()){
                    maxArea = countries.get(i);
                }
                if(countries.get(i).getPopulation() / countries.get(i).getArea() >
                        maxPopulationDensity.getPopulation() / maxPopulationDensity.getArea()) {
                    maxPopulationDensity = countries.get(i);
                }
            }

            // Print results to console
            System.out.println("Country with largest population: " + maxPopulation.getName());
            System.out.println("Country with largest area: " + maxArea.getName());
            System.out.println("Country with largest population density: " + maxPopulationDensity.getName());

        }
    }

}
