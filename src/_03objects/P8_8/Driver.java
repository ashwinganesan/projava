package _03objects.P8_8;

public class Driver {

    public static void main(String[] args) {

        // Create new voting machine object
        VotingMachine election2020 = new VotingMachine();

        // Flip coin 10000 times to vote for democrat or republican
        for (int i = 0; i < 10000; i++){
            if(Math.random() >= 0.5){
                election2020.voteDemocrat();
            }
            else{
                election2020.voteRepublican();
            }
        }

        // Print tallies
        election2020.getTallies();

    }

}
