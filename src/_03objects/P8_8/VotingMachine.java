package _03objects.P8_8;

public class VotingMachine {

    // Initialize int variables for number of democrat and republican votes
    private int democratVotes;
    private int republicanVotes;

    // Constructor
    public VotingMachine(){
        this.clear();
    }

    // Clear tallies
    public void clear() {
        this.democratVotes = 0;
        this.republicanVotes = 0;
    }

    // Vote for democrat
    public void voteDemocrat(){
        democratVotes += 1;
    }

    // Vote for republican
    public void voteRepublican(){
        republicanVotes += 1;
    }

    // Print tallies to console
    public void getTallies(){
        System.out.println("Democrats: " + democratVotes + ", Republicans: " + republicanVotes);
    }
}
