package _03objects.P8_7;

public class Driver {

    public static void main(String[] args) {

        // Create new ComboLock object
        ComboLock unlockMe = new ComboLock(11, 36, 20);

        // Enter wrong combination
        unlockMe.turnRight(26);
        unlockMe.turnLeft(18);
        unlockMe.turnRight(25);
        System.out.println(unlockMe.open());

        // Enter correct combination
        unlockMe.reset();
        unlockMe.turnRight(11);
        unlockMe.turnLeft(15);
        unlockMe.turnRight(24);
        System.out.println(unlockMe.open());

    }

}
