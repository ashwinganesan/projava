package _03objects.P8_7;

import java.util.ArrayList;

public class ComboLock {

    // Range of numbers on lock = 40 (0 to 39)
    private static final int NUM_RANGE = 40;

    // Current location of dial pointer
    private int dialPointsTo;

    // Int array containing correct combination
    private int[] secrets = new int[3];

    // Integer arraylist containing last 3 inputs at any point
    private ArrayList<Integer> inputs = new ArrayList<>();

    // Boolean arraylist containing whether last 3 turns were to the right at any point
    private ArrayList<Boolean> right = new ArrayList<>();

    // Constructor
    public ComboLock(int secret1, int secret2, int secret3){
        this.reset();
        secrets[0] = secret1;
        secrets[1] = secret2;
        secrets[2] = secret3;
        for(int i = 0; i < 3; i++){
            inputs.add(0);
            right.add(false);
        }
    }

    // Reset dial pointer to 0
    public void reset(){
        dialPointsTo = 0;
    }

    // Turn left by certain number of ticks
    public void turnLeft(int ticks){

        // Calculate pointer position considering overflow
        dialPointsTo = NUM_RANGE + (dialPointsTo - ticks) % NUM_RANGE;

        // Update right arraylist
        right.remove(0);
        right.add(false);

        // Update inputs arraylist
        inputs.remove(0);
        inputs.add(dialPointsTo);
        System.out.println(dialPointsTo);
    }

    // Turn right by certain number of ticks
    public void turnRight(int ticks){

        // Calculate pointer position considering overflow
        dialPointsTo = (dialPointsTo + ticks) % NUM_RANGE;

        // Update right arraylist
        right.remove(0);
        right.add(true);

        // Update inputs arraylist
        inputs.remove(0);
        inputs.add(dialPointsTo);
        System.out.println(dialPointsTo);

    }

    // Return true if combination was correct and turns were in the order (right, left, right)
    public boolean open(){
        return right.get(0).equals(true) && right.get(1).equals(false) && right.get(2).equals(true) &&
                inputs.get(0).equals(secrets[0]) && inputs.get(1).equals(secrets[1]) && inputs.get(2).equals(secrets[2]);
    }

}
