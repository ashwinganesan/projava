package _03objects.P8_6;

public class Driver {

    public static void main(String[] args) {

        // Create new car object
        Car gasGuzzler = new Car(12);

        // Simulate adding fuel and driving and print remaining fuel
        gasGuzzler.addGas(20);
        gasGuzzler.drive(150);
        System.out.println("Gas guzzler has " + gasGuzzler.getGasLevel() + " gallons of fuel left.");

    }

}
