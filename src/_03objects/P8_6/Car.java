package _03objects.P8_6;

public class Car {

    // Initialize fuel efficiency and fuel in tank
    public double fuelEfficiency;
    public double fuelInTank;

    // Constructor
    public Car(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
        this.fuelInTank = 0;
    }

    // Simulate driving and reduce fuel in tank
    public void drive(double distance){
        fuelInTank = fuelInTank - distance / fuelEfficiency;
    }

    // Get current fuel level
    public double getGasLevel() {
        return fuelInTank;
    }

    // Add fuel to tank
    public void addGas(double fuel){
        fuelInTank += fuel;
    }

}
