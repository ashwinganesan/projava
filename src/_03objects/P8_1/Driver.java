package _03objects.P8_1;

public class Driver {

    public static void main(String[] args) {

        // Create microwave object
        Microwave bosch = new Microwave();

        // Cook for 60 seconds at power level 1
        bosch.increaseCookingTime();
        bosch.increaseCookingTime();
        bosch.start();

        // Reset and cook for 30 seconds at power level 2
        bosch.reset();
        bosch.increaseCookingTime();
        bosch.switchPowerLevel();
        bosch.start();

    }

}
