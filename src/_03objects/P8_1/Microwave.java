package _03objects.P8_1;

public class Microwave {

    // Initialize cooking time and power level
    private int cookingTime;
    private int powerLevel;

    // Constructor
    public Microwave() {
        this.reset();
    }

    // Increase cooking time by 30 seconds
    public void increaseCookingTime(){
        this.cookingTime += 30;
    }

    // Switch power level between 1 and 2
    public void switchPowerLevel(){
        if(this.powerLevel == 1){
            this.powerLevel = 2;
        }
        else{
            this.powerLevel = 1;
        }
    }

    // Reset cooking time to 0 and power level to 1
    public void reset(){
        this.cookingTime = 0;
        this.powerLevel = 1;
    }

    // Start cooking
    public void start(){
        System.out.println("Cooking for " + cookingTime + " seconds at level " + powerLevel);
    }
}
