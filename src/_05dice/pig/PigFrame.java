package _05dice.pig;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class PigFrame extends JFrame {

    // Initialize frame size and text area size
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 300;
    private static final int AREA_ROWS = 10;
    private static final int AREA_COLUMNS = 30;

    // Initialize roll and hold buttons
    private JButton rollButton;
    private JButton holdButton;

    // Initialize score labels
    private JLabel userScoreLabel;
    private JLabel compScoreLabel;
    private JLabel userTurnScoreLabel;
    private JLabel compTurnScoreLabel;

    // Initialize text area and pig image label
    private JTextArea rollsArea;
    private JLabel pigLabel;

    // Initialize total scores for user and computer
    private int userScore;
    private int compScore;

    // Initialize turn scores for user and computer
    private int userTurnScore;
    private int compTurnScore;

    // True if user's turn
    private boolean userTurn;

    // True if game has ended
    private boolean gameEnd;

    // True if user has rolled at least once
    private boolean userRolled;

    // Constructor
    public PigFrame() {
        userScore = 0;
        compScore = 0;
        userTurnScore = 0;
        compTurnScore = 0;
        userTurn = true;
        gameEnd = false;
        userRolled = false;

        rollsArea = new JTextArea(AREA_ROWS, AREA_COLUMNS);
        rollsArea.setEditable(false);
        rollsArea.append("Click Roll to start.\n");

        createButtons();
        createLabels();
        createPanel();

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    // Action listener for roll button
    class RollButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (userTurn && !gameEnd) {
                userRolled = true;

                // Random value from 1 to 6
                int rollValue = 1 + (int) (Math.random() * 6);
                rollsArea.append("User rolls " + rollValue + ".\n");

                // Reset turn score and end turn if 1 is rolled
                if (rollValue == 1) {
                    userTurnScore = 0;
                    userTurn = false;
                    rollsArea.append("User turn ends.\n");
                    compTurnScore = 0;
                    compTurn();
                } else {
                    // Add turn score to total score
                    userTurnScore += rollValue;
                }
                userTurnScoreLabel.setText("User turn score: " + userTurnScore);

                // End game if total score reaches 100
                if (userScore + userTurnScore >= 100) {
                    gameEnd = true;
                    rollsArea.append("USER WINS!");
                }
            }

        }
    }

    // Action listener for hold button
    class HoldButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            // Add turn score to total score and run computer's turn
            if (userTurn && !gameEnd && userRolled) {
                userScore += userTurnScore;
                userScoreLabel.setText("User total score: " + userScore);
                userTurn = false;
                rollsArea.append("User holds.\n");
                compTurnScore = 0;
                compTurn();
            }

        }
    }

    // Computer's turn
    private void compTurn() {

        // Set random number of rolls from 1 to 10
        int numRolls = 1 + (int) (Math.random() * 10);

        // Initialize roll counter
        int currentRoll = 1;

        // Simulate computer's turn
        while (!userTurn && !gameEnd && currentRoll <= numRolls) {

            // Random value from 1 to 6
            int rollValue = 1 + (int) (Math.random() * 6);
            rollsArea.append("Computer rolls " + rollValue + ".\n");

            // Reset turn score and end turn if 1 is rolled
            if (rollValue == 1) {
                compTurnScore = 0;
                userTurn = true;
                userRolled = false;
                rollsArea.append("Computer turn ends.\n");
                userTurnScore = 0;
            } else {
                // Add turn score to total score
                compTurnScore += rollValue;
            }
            compTurnScoreLabel.setText("Computer turn score: " + compTurnScore);

            // End game if total score reaches 100
            if (compScore + compTurnScore >= 100) {
                gameEnd = true;
                rollsArea.append("COMPUTER WINS!");
            }

            // Increment roll counter
            if (!userTurn) {
                currentRoll++;
            }

        }

        // Simulate hold
        if (!gameEnd && currentRoll > numRolls) {
            // Add turn score to total score and end computer's turn
            compScore += compTurnScore;
            compScoreLabel.setText("Computer total score: " + compScore);
            userTurn = true;
            userRolled = false;
            rollsArea.append("Computer holds.\n");
            userTurnScore = 0;
        }

    }

    // Create roll and hold buttons
    private void createButtons() {

        rollButton = new JButton("Roll");
        rollButton.addActionListener(new RollButtonListener());
        holdButton = new JButton("Hold");
        holdButton.addActionListener(new HoldButtonListener());

    }

    // Create labels for scores and pig image
    private void createLabels(){

        userTurnScoreLabel = new JLabel("User turn score: " + userTurnScore);
        compTurnScoreLabel = new JLabel("Computer turn score: " + compTurnScore);
        userScoreLabel = new JLabel("User total score: " + userScore);
        compScoreLabel = new JLabel("Computer total score: " + compScore);

        try{
            BufferedImage pigImage = ImageIO.read(this.getClass().getResource("pig.jpg"));
            pigLabel = new JLabel(new ImageIcon(pigImage));
        } catch (Exception e) {
            pigLabel = new JLabel("");
        }

    }

    // Create panel
    private void createPanel(){

        JPanel panel = new JPanel();
        panel.add(rollButton);
        panel.add(holdButton);
        panel.add(userTurnScoreLabel);
        panel.add(userScoreLabel);
        JScrollPane scrollPane = new JScrollPane(rollsArea);
        panel.add(scrollPane);
        panel.add(compTurnScoreLabel);
        panel.add(compScoreLabel);
        panel.add(pigLabel);
        add(panel);

    }

}
