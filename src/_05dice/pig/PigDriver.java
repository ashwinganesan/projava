package _05dice.pig;

import javax.swing.*;

public class PigDriver {

    public static void main(String[] args) {

        // Create frame
        JFrame frame = new PigFrame();
        frame.setTitle("PIG GAME");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);

    }
}
