package _05dice.P10_10;

import javax.swing.*;

public class Driver {

    public static void main(String[] args) {

        // Create frame
        JFrame frame = new JFrame();
        frame.setSize(400, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create component and add to frame
        JComponent component = new OlympicRingsComponent();
        frame.add(component);

        frame.setVisible(true);

    }

}
