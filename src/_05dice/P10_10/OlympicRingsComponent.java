package _05dice.P10_10;

import javax.swing.*;
import java.awt.*;

public class OlympicRingsComponent extends JComponent {

    public void paintComponent(Graphics g) {

        // Convert Graphics object to Graphics2D to facilitate setting stroke width
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(5));

        // Draw olympic rings
        drawRing(g2d, 10, 10,25, new Color(0, 133, 199));
        drawRing(g2d, 60, 10,25, Color.BLACK);
        drawRing(g2d, 110, 10,25, new Color(223, 0, 36));
        drawRing(g2d, 35, 35,25, new Color(244, 195, 0));
        drawRing(g2d, 85, 35,25, new Color(0, 159, 61));

    }

    void drawRing(Graphics2D g2d, int xLeft, int yTop, int radius, Color color) {

        // Draw ring with given position and color
        g2d.setColor(color);
        g2d.drawOval(xLeft, yTop, 2 * radius, 2 * radius);

    }

}
