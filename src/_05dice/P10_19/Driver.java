package _05dice.P10_19;

import javax.swing.*;

public class Driver {

    public static void main(String[] args) {

        // Create frame
        JFrame frame = new RestaurantBillFrame();
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);

    }

}
