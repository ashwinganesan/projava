package _05dice.P10_19;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RestaurantBillFrame extends JFrame{

    // Initialize frame size
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 250;

    // Initialize text area size
    private static final int AREA_ROWS = 10;
    private static final int AREA_COLUMNS = 30;

    // Initialize tax and tip rates
    private static final double TAX_RATE = 10;
    private static final double TIP_RATE = 15;

    // Initialize popular item buttons
    private JButton pizzaButton;
    private JButton pastaButton;
    private JButton riceButton;
    private JButton dumplingsButton;
    private JButton tacoButton;
    private JButton burritoButton;
    private JButton curryButton;
    private JButton schnitzelButton;
    private JButton sushiButton;
    private JButton soupButton;

    // Initialize labels, fields and button for adding new items
    private JLabel nameLabel;
    private JTextField nameField;
    private JLabel priceLabel;
    private JTextField priceField;
    private JButton addNewItemButton;

    // Initialize text area, tip and tax labels and total
    private JTextArea billArea;
    private JLabel taxLabel;
    private JLabel tipLabel;
    private double total;

    // Constructor
    public RestaurantBillFrame() {

        total = 0;
        billArea = new JTextArea(AREA_ROWS, AREA_COLUMNS);
        billArea.setEditable(false);

        createPopularItemButtons();
        createTextFields();
        createTaxTipLabels();
        createPanel();

        setSize(FRAME_WIDTH, FRAME_HEIGHT);

    }

    // Action listener for adding popular items
    class AddPopularItemListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String item = ((JButton) e.getSource()).getText();
            if (item.equals("Pizza")) {
                total += 18;
            }
            else if (item.equals("Pasta")) {
                total += 12;
            }
            else if (item.equals("Rice")) {
                total += 5;
            }
            else if (item.equals("Dumplings")) {
                total += 15;
            }
            else if (item.equals("Taco")) {
                total += 8;
            }
            else if (item.equals("Burrito")) {
                total += 10;
            }
            else if (item.equals("Curry")) {
                total += 10;
            }
            else if (item.equals("Schnitzel")) {
                total += 20;
            }
            else if (item.equals("Sushi")) {
                total += 25;
            }
            else if (item.equals("Soup")) {
                total += 5;
            }

            billArea.append("Added " + item + ". Total: $" + String.format("%.2f", total * (1 + TAX_RATE / 100) * (1 + TIP_RATE / 100)) + "\n");

        }
    }

    // Action listener for adding new items
    class AddNewItemListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            total += Double.parseDouble(priceField.getText());
            billArea.append("Added " + nameField.getText() + ". Total: $" + String.format("%.2f", total * (1 + TAX_RATE / 100) * (1 + TIP_RATE / 100)) + "\n");
        }

    }

    // Create popular item buttons
    private void createPopularItemButtons() {
        pizzaButton = new JButton("Pizza");
        pizzaButton.addActionListener(new AddPopularItemListener());
        pastaButton = new JButton("Pasta");
        pastaButton.addActionListener(new AddPopularItemListener());
        riceButton = new JButton("Rice");
        riceButton.addActionListener(new AddPopularItemListener());
        dumplingsButton = new JButton("Dumplings");
        dumplingsButton.addActionListener(new AddPopularItemListener());
        tacoButton = new JButton("Taco");
        tacoButton.addActionListener(new AddPopularItemListener());
        burritoButton = new JButton("Burrito");
        burritoButton.addActionListener(new AddPopularItemListener());
        curryButton = new JButton("Curry");
        curryButton.addActionListener(new AddPopularItemListener());
        schnitzelButton = new JButton("Schnitzel");
        schnitzelButton.addActionListener(new AddPopularItemListener());
        sushiButton = new JButton("Sushi");
        sushiButton.addActionListener(new AddPopularItemListener());
        soupButton = new JButton("Soup");
        soupButton.addActionListener(new AddPopularItemListener());
    }

    // Create new item name and price labels and text fields
    private void createTextFields() {

        nameLabel = new JLabel("Item name: ");
        priceLabel = new JLabel("Item price: ");

        final int NAME_FIELD_WIDTH = 25;
        final int PRICE_FIELD_WIDTH = 10;

        nameField = new JTextField(NAME_FIELD_WIDTH);
        priceField = new JTextField(PRICE_FIELD_WIDTH);

        addNewItemButton = new JButton("Add item");
        addNewItemButton.addActionListener(new AddNewItemListener());

    }

    // Create tax and tip labels
    private void createTaxTipLabels() {

        taxLabel = new JLabel("Tax rate: " + TAX_RATE + "%");
        tipLabel = new JLabel("Suggested tip rate: " + TIP_RATE + "%");

    }

    // Create panel
    private void createPanel() {
        JPanel panel = new JPanel();
        panel.add(pizzaButton);
        panel.add(pastaButton);
        panel.add(riceButton);
        panel.add(dumplingsButton);
        panel.add(tacoButton);
        panel.add(burritoButton);
        panel.add(curryButton);
        panel.add(schnitzelButton);
        panel.add(sushiButton);
        panel.add(soupButton);
        panel.add(nameLabel);
        panel.add(nameField);
        panel.add(priceLabel);
        panel.add(priceField);
        panel.add(addNewItemButton);
        JScrollPane scrollPane = new JScrollPane(billArea);
        panel.add(scrollPane);
        panel.add(taxLabel);
        panel.add(tipLabel);
        add(panel);
    }

}