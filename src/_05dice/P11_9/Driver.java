package _05dice.P11_9;

import javax.swing.*;

public class Driver {

    public static void main(String[] args) {

        // Create frame
        JFrame frame = new DrawCircleFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);

    }

}
