package _05dice.P11_9;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class DrawCircleFrame extends JFrame {

    // Initialize frame size
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 400;

    // Initialize center position and boolean indicator
    private boolean hasCenter;
    private int xCenter;
    private int yCenter;

    // Initialize circle component
    private CircleComponent scene;

    // Mouse listener for center and periphery point
    class MousePressListener implements MouseListener {

        public void mousePressed(MouseEvent e) {

            int x = e.getX();
            int y = e.getY();

            // Flip boolean and draw circle or set center depending on value
            hasCenter = !hasCenter;
            if (hasCenter) {
                scene.drawCircleAt(xCenter, yCenter, x, y);
            }
            else {
                xCenter = x;
                yCenter = y;
            }
        }

        public void mouseClicked(MouseEvent e) {}
        public void mouseEntered(MouseEvent e) {}
        public void mouseExited(MouseEvent e) {}
        public void mouseReleased(MouseEvent e) {}

    }

    // Constructor
    public DrawCircleFrame() {
        hasCenter = true;
        scene = new CircleComponent();
        add(scene);
        scene.addMouseListener(new MousePressListener());
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

}
