package _05dice.P11_9;

import javax.swing.*;
import java.awt.*;

public class CircleComponent extends JComponent {

    // Initialize position and radius
    private int xLeft;
    private int yTop;
    private int radius;

    // Constructor
    public CircleComponent() {
        xLeft = 0;
        yTop = 0;
        radius = 0;
    }

    // Draw circle
    public void paintComponent(Graphics g) {
        g.drawOval(xLeft, yTop, 2 * radius, 2 * radius);
    }

    // Set position and radius for given center and periphery point
    public void drawCircleAt(int xCenter, int yCenter, int xPeriphery, int yPeriphery) {
        radius = (int) Math.sqrt(Math.pow(xPeriphery - xCenter, 2) + Math.pow(yPeriphery - yCenter, 2));
        xLeft = xCenter - radius;
        yTop = yCenter - radius;
        repaint();
    }

}
