package _05dice.P10_9;

import javax.swing.*;

public class Driver {

    public static void main(String[] args) {

        // Create frame
        JFrame frame = new JFrame();
        frame.setSize(300, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create component and add to frame
        JComponent component = new HorizontalStripedFlagComponent();
        frame.add(component);

        frame.setVisible(true);

    }

}
