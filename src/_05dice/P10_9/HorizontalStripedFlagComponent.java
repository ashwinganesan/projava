package _05dice.P10_9;

import javax.swing.*;
import java.awt.*;

public class HorizontalStripedFlagComponent extends JComponent {

    public void paintComponent(Graphics g) {

        // Draw German flag
        drawHorizontalStripedFlag(g, 10, 10, 150, Color.BLACK, new Color(218, 41, 28), new Color(255, 205, 0));

        // Draw Hungarian flag
        drawHorizontalStripedFlag(g, 10, 125, 150, new Color(200, 16, 46), Color.WHITE, new Color(0, 132, 61));

    }

    void drawHorizontalStripedFlag(Graphics g, int xLeft, int yTop, int width, Color top, Color middle, Color bottom) {

        // Draw top rectangle
        g.setColor(top);
        g.fillRect(xLeft, yTop, width, width * 2 / 9);

        // Draw middle rectangle
        g.setColor(middle);
        g.fillRect(xLeft, yTop + width * 2 / 9, width, width * 2 / 9);

        // Draw bottom rectangle
        g.setColor(bottom);
        g.fillRect(xLeft, yTop + width * 4 / 9, width, width * 2 / 9);

    }

}
